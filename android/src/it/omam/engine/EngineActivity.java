package it.omam.engine;

import android.app.NativeActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class EngineActivity extends NativeActivity
{
    private AdView adView;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        
        Log.v("omam", "init");
        LinearLayout ll = new LinearLayout(this);
        TextView tv = new TextView(this);
        tv.setText("ciao");
        ll.addView(tv);
        setContentView(ll);

        // Create an ad.
        adView = new AdView(this, AdSize.BANNER, "a150f6d70d674fb");

        // Add the AdView to the view hierarchy. The view will have no size
        // until the ad is loaded.
        ll.addView(adView);

        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device.
        AdRequest adRequest = new AdRequest();
        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);

        // Start loading the ad in the background.
        adView.loadAd(adRequest);
    }
}