LOCAL_PATH := $(call my-dir)/../../src/

include $(CLEAR_VARS)
LOCAL_MODULE := png
LOCAL_SRC_FILES := \
	png/png.c \
	png/pngerror.c \
	png/pngget.c \
	png/pngmem.c \
	png/pngpread.c \
	png/pngread.c \
	png/pngrio.c \
	png/pngrtran.c \
	png/pngrutil.c \
	png/pngset.c \
	png/pngtest.c \
	png/pngtrans.c \
	png/pngwio.c \
	png/pngwrite.c \
	png/pngwtran.c \
	png/pngwutil.c
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/png
LOCAL_EXPORT_LDLIBS := -lz
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := box2d
LOCAL_SRC_FILES := \
    Box2D/Collision/b2BroadPhase.cpp \
    Box2D/Collision/b2CollideCircle.cpp \
    Box2D/Collision/b2CollideEdge.cpp \
    Box2D/Collision/b2CollidePolygon.cpp \
    Box2D/Collision/b2Collision.cpp \
    Box2D/Collision/b2Distance.cpp \
    Box2D/Collision/b2DynamicTree.cpp \
    Box2D/Collision/b2TimeOfImpact.cpp \
    Box2D/Collision/Shapes/b2ChainShape.cpp \
    Box2D/Collision/Shapes/b2CircleShape.cpp \
    Box2D/Collision/Shapes/b2EdgeShape.cpp \
    Box2D/Collision/Shapes/b2PolygonShape.cpp \
    Box2D/Common/b2BlockAllocator.cpp \
    Box2D/Common/b2Draw.cpp \
    Box2D/Common/b2Math.cpp \
    Box2D/Common/b2Settings.cpp \
    Box2D/Common/b2StackAllocator.cpp \
    Box2D/Common/b2Timer.cpp \
    Box2D/Dynamics/b2Body.cpp \
    Box2D/Dynamics/b2ContactManager.cpp \
    Box2D/Dynamics/b2Fixture.cpp \
    Box2D/Dynamics/b2Island.cpp \
    Box2D/Dynamics/b2World.cpp \
    Box2D/Dynamics/b2WorldCallbacks.cpp \
    Box2D/Dynamics/Contacts/b2ChainAndCircleContact.cpp \
    Box2D/Dynamics/Contacts/b2ChainAndPolygonContact.cpp \
    Box2D/Dynamics/Contacts/b2CircleContact.cpp \
    Box2D/Dynamics/Contacts/b2Contact.cpp \
    Box2D/Dynamics/Contacts/b2ContactSolver.cpp \
    Box2D/Dynamics/Contacts/b2EdgeAndCircleContact.cpp \
    Box2D/Dynamics/Contacts/b2EdgeAndPolygonContact.cpp \
    Box2D/Dynamics/Contacts/b2PolygonAndCircleContact.cpp \
    Box2D/Dynamics/Contacts/b2PolygonContact.cpp \
    Box2D/Dynamics/Joints/b2DistanceJoint.cpp \
    Box2D/Dynamics/Joints/b2FrictionJoint.cpp \
    Box2D/Dynamics/Joints/b2GearJoint.cpp \
    Box2D/Dynamics/Joints/b2Joint.cpp \
    Box2D/Dynamics/Joints/b2MouseJoint.cpp \
    Box2D/Dynamics/Joints/b2PrismaticJoint.cpp \
    Box2D/Dynamics/Joints/b2PulleyJoint.cpp \
    Box2D/Dynamics/Joints/b2RevoluteJoint.cpp \
    Box2D/Dynamics/Joints/b2RopeJoint.cpp \
    Box2D/Dynamics/Joints/b2WeldJoint.cpp \
    Box2D/Dynamics/Joints/b2WheelJoint.cpp \
    Box2D/Rope/b2Rope.cpp
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/box2d
LOCAL_EXPORT_LDLIBS := -lz
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := tremolo
LOCAL_SRC_FILES := \
	tremolo/annotate.c \
	tremolo/bitwise.c \
	tremolo/bitwiseARM.s \
	tremolo/codebook.c \
	tremolo/dpen.s \
	tremolo/dsp.c \
	tremolo/floor0.c \
	tremolo/floor1.c \
	tremolo/floor1ARM.s \
	tremolo/floor1LARM.s \
	tremolo/floor_lookup.c \
	tremolo/framing.c \
	tremolo/info.c \
	tremolo/mapping0.c \
	tremolo/mdct.c \
	tremolo/mdctARM.s \
	tremolo/mdctLARM.s \
	tremolo/misc.c \
	tremolo/res012.c \
	tremolo/speed.s \
	tremolo/vorbisfile.c
LOCAL_CFLAGS := -O2 -D_ARM_ASSEM_
LOCAL_ARM_MODE := arm
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/tremolo
LOCAL_EXPORT_LDLIBS := -lz
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := jpeg
LOCAL_SRC_FILES := \
    jpeg/jcapimin.c \
    jpeg/jcapistd.c \
    jpeg/jccoefct.c \
    jpeg/jccolor.c \
    jpeg/jcdctmgr.c \
    jpeg/jchuff.c \
    jpeg/jcinit.c \
    jpeg/jcmainct.c \
    jpeg/jcmarker.c \
    jpeg/jcmaster.c \
    jpeg/jcomapi.c \
    jpeg/jcparam.c \
    jpeg/jcphuff.c \
    jpeg/jcprepct.c \
    jpeg/jcsample.c \
    jpeg/jctrans.c \
    jpeg/jdapimin.c \
    jpeg/jdapistd.c \
    jpeg/jdatadst.c \
    jpeg/jdatasrc.c \
    jpeg/jdcoefct.c \
    jpeg/jdcolor.c \
    jpeg/jddctmgr.c \
    jpeg/jdhuff.c \
    jpeg/jdinput.c \
    jpeg/jdmainct.c \
    jpeg/jdmarker.c \
    jpeg/jdmaster.c \
    jpeg/jdmerge.c \
    jpeg/jdphuff.c \
    jpeg/jdpostct.c \
    jpeg/jdsample.c \
    jpeg/jdtrans.c \
    jpeg/jerror.c \
    jpeg/jfdctflt.c \
    jpeg/jfdctfst.c \
    jpeg/jfdctint.c \
    jpeg/jidctflt.c \
    jpeg/jidctfst.c \
    jpeg/jidctint.c \
    jpeg/jidctred.c \
    jpeg/jmemansi.c \
    jpeg/jmemmgr.c \
    jpeg/jmemname.c \
    jpeg/jmemnobs.c \
    jpeg/jpegtran.c \
    jpeg/jquant1.c \
    jpeg/jquant2.c \
    jpeg/jutils.c \
    jpeg/rdbmp.c \
    jpeg/rdcolmap.c \
    jpeg/rdgif.c \
    jpeg/rdjpgcom.c \
    jpeg/rdppm.c \
    jpeg/rdrle.c \
    jpeg/rdswitch.c \
    jpeg/rdtarga.c \
    jpeg/transupp.c \
    jpeg/wrbmp.c \
    jpeg/wrgif.c \
    jpeg/wrjpgcom.c \
    jpeg/wrppm.c \
    jpeg/wrrle.c \
    jpeg/wrtarga.c

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/jpeg
LOCAL_EXPORT_LDLIBS := -lz
include $(BUILD_STATIC_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE    := untangle
LOCAL_SRC_FILES := \
    test/gfx_main.cpp \
    engine_3dmath.cpp \
	engine_gui.cpp \
	engine_primitives.cpp \
	engine_scene.cpp \
	engine_texture.cpp \
    android_audio.c \
    android_events.c \
    android_file.c \
    android_ipc.c \
    android_main.c \
    android_touch.c \
    android_video.c \
    texture.c \
    time.c 
LOCAL_LDLIBS    := -llog -landroid -lEGL -lGLESv1_CM -lz -lOpenSLES
LOCAL_CFLAGS    := -DENGINE_ANDROID -I$(LOCAL_PATH)
LOCAL_STATIC_LIBRARIES := jpeg box2d tremolo png
include $(BUILD_SHARED_LIBRARY)
