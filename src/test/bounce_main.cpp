#include "engine.h"
#include <math.h>

static float randf()
{
    static float div = 1.0f / (float) RAND_MAX;
    return (float) (rand() % RAND_MAX) * div;
}

static float randfneg()
{
    static float div = 2.0f / (float) RAND_MAX;
    return (float) (rand() % RAND_MAX) * div - 1.0f;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Matrix Algebra
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct vertex_t
{
    float x, y;
} vertex_t;

typedef struct matrix_t
{
    float m11, m12, m13;
    float m21, m22, m23;
    float m31, m32, m33;
} matrix_t;

static void draw_point(vertex_t *v)
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, v);
    glDrawArrays(GL_POINTS, 0, 1);
    glDisableClientState(GL_VERTEX_ARRAY);
}

static void draw_line(vertex_t *v1, vertex_t *v2)
{
    static vertex_t v[2];
    v[0] = *v1;
    v[1] = *v2;
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(2, GL_FLOAT, 0, v);
    glDrawArrays(GL_LINES, 0, 2);
    glDisableClientState(GL_VERTEX_ARRAY);
}

static matrix_t matrix_mul(matrix_t *a, matrix_t *b)
{
    matrix_t res;
    res.m11 = (a->m11 * b->m11) + (a->m12 * b->m21) + (a->m13 * b->m31);
    res.m12 = (a->m11 * b->m12) + (a->m12 * b->m22) + (a->m13 * b->m32);
    res.m13 = (a->m11 * b->m13) + (a->m12 * b->m23) + (a->m13 * b->m33);
    res.m21 = (a->m21 * b->m11) + (a->m22 * b->m21) + (a->m23 * b->m31);
    res.m22 = (a->m21 * b->m12) + (a->m22 * b->m22) + (a->m23 * b->m32);
    res.m23 = (a->m21 * b->m13) + (a->m22 * b->m23) + (a->m23 * b->m33);
    res.m31 = (a->m31 * b->m11) + (a->m32 * b->m21) + (a->m33 * b->m31);
    res.m32 = (a->m31 * b->m12) + (a->m32 * b->m22) + (a->m33 * b->m32);
    res.m33 = (a->m31 * b->m13) + (a->m32 * b->m23) + (a->m33 * b->m33);
    return res;
}

static vertex_t vertex_mul(vertex_t *a, matrix_t *b)
{
    vertex_t res;
    res.x = (a->x * b->m11) + (a->y * b->m21) + b->m31;
    res.y = (a->x * b->m12) + (a->y * b->m22) + b->m32;
    return res;
}

static vertex_t vertex_mul(vertex_t *v, float f)
{
    return (vertex_t){v->x * f, v->y * f};
}

static float vertex_dot(vertex_t *a, vertex_t *b)
{
    return a->x * b->x + a->y * b->y;
}

static vertex_t vertex_diff(vertex_t *a, vertex_t *b)
{
    return (vertex_t){a->x - b->x, a->y - b->y};
}

static vertex_t vertex_sum(vertex_t *a, vertex_t *b)
{
    return (vertex_t){a->x + b->x, a->y + b->y};
}

static vertex_t vertex_normal(vertex_t *a)
{
    return (vertex_t){-a->y, a->x};
}

static vertex_t vertex_normalize(vertex_t *a)
{
    float dist = 1.0f / sqrtf(a->x * a->x + a->y * a->y);
    return (vertex_t){a->x * dist, a->y * dist};
}

static float vertex_distq(vertex_t *a, vertex_t *b)
{
    float dx = a->x - b->x;
    float dy = a->y - b->y;
    return dx * dx + dy * dy;
}

static float vertex_dist(vertex_t *a, vertex_t *b)
{
    float dx = a->x - b->x;
    float dy = a->y - b->y;
    return sqrtf(dx * dx + dy * dy);
}

static float vertex_norm(vertex_t *a)
{
    return sqrtf(a->x * a->x + a->y * a->y);
}

static int line_intersect(vertex_t *a, vertex_t *b, vertex_t *c, vertex_t *d, vertex_t *out)
{
    vertex_t e = vertex_diff(b, a);
    vertex_t f = vertex_diff(d, c);
    vertex_t g = vertex_diff(a, c);
    vertex_t p = (vertex_t){-e.y, e.x};
    vertex_t q = (vertex_t){-f.y, f.x};
    float dot_fp = vertex_dot(&f, &p);
    if(dot_fp == 0.0f) return 0;
    float dot_gp = vertex_dot(&g, &p);
    float dot_gq = vertex_dot(&g, &q);
    float h = dot_gp / dot_fp;
    float t = dot_gq / dot_fp;
    out->x = c->x + f.x * h;
    out->y = c->y + f.y * h;
    return (h <= 1.0f && h >= 0.0f) && (t >= 0.0f && t <= 1.0f);
}

static matrix_t matrix_zero()
{
    return (matrix_t){0};
}

static matrix_t matrix_identity()
{
    return (matrix_t){1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
}

static matrix_t matrix_transpose(matrix_t *m)
{
    matrix_t res;
    res.m11 = m->m11;
    res.m12 = m->m21;
    res.m13 = m->m31;
    res.m21 = m->m12;
    res.m22 = m->m22;
    res.m23 = m->m32;
    res.m31 = m->m13;
    res.m32 = m->m23;
    res.m33 = m->m33;
    return res;
}

static matrix_t matrix_invert(matrix_t *m)
{
    matrix_t res = *m;
    float sx = 1.0f / (m->m11 * m->m11 + m->m12 * m->m12);
    float sy = 1.0f / (m->m21 * m->m21 + m->m22 * m->m22);
    res.m11 = m->m11 * sx;
    res.m12 = m->m21 * sy;
    res.m21 = m->m12 * sx;
    res.m22 = m->m22 * sy;
    res.m31 = (-m->m31 * m->m11 - m->m32 * -m->m12) * sx;
    res.m32 = (-m->m31 * m->m21 - m->m32 * -m->m22) * sy;
    return res;
}

static matrix_t matrix_rotation(float theta)
{
    float s = sinf(theta);
    float c = cosf(theta);
    return (matrix_t){c, s, 0.0f, -s, c, 0.0f, 0.0f, 0.0f, 1.0f};
}

static matrix_t matrix_translation(float tx, float ty)
{
    return (matrix_t){1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, tx, ty, 1.0f};
}

static matrix_t matrix_scaling(float sx, float sy)
{
    return (matrix_t){sx, 0.0f, 0.0f, 0.0f, sy, 0.0f, 0.0f, 0.0f, 1.0f};
}

static inline void matrix_translate(matrix_t *m, float tx, float ty)
{
    m->m31 += tx;
    m->m32 += ty;
}

static inline void matrix_rotate(matrix_t *m, float theta)
{
    float s = sinf(theta);
    float c = cosf(theta);
    m->m11 = c;
    m->m12 = s;
    m->m21 = -s;
    m->m22 = c;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Box
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Point2D
{
public:
    vertex_t vel, pos;
    
    Point2D()
    {
        vel = (vertex_t){-0.008f, 0.01f};
        pos = (vertex_t){0};
    }
};

class Box2D
{
private:
    unsigned char dirty;
    vertex_t pos, vel, acc, sz, rot;
    matrix_t m, inv;
public:
    Box2D()
    {
        dirty = 0;
        identity();
    }
    void identity()
    {
        m = inv = matrix_identity();
    }
    void rotate(float theta)
    {
        matrix_t r;
        r = matrix_rotation(theta);
        m = matrix_mul(&m, &r);
        r = matrix_transpose(&r);
        inv = matrix_mul(&r, &inv);
    }
    void translate(float tx, float ty)
    {
        matrix_t t;
        t = matrix_translation(tx, ty);
        m = matrix_mul(&m, &t);
        t = matrix_translation(-tx, -ty);
        inv = matrix_mul(&t, &inv);
    }
    void scale(float sx, float sy)
    {
        matrix_t s;
        s = matrix_scaling(sx, sy);
        m = matrix_mul(&m, &s);
        s = matrix_scaling(1.0f / sx, 1.0f / sy);
        inv = matrix_mul(&s, &inv);
    }
    vertex_t project(vertex_t *v)
    {
        return vertex_mul(v, &m);
    }
    vertex_t unproject(vertex_t *v)
    {
        return vertex_mul(v, &inv);
    }
    void draw()
    {
        static vertex_t v[4] = {-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
        
        vertex_t project[4];
        int i;
        
        for(i = 0; i < 4; i++)
            project[i] = vertex_mul(&v[i], &m);

        glEnableClientState(GL_VERTEX_ARRAY);
        glVertexPointer(2, GL_FLOAT, 0, project);
        glDrawArrays(GL_POINTS, 0, 4);
        glDrawArrays(GL_LINE_LOOP, 0, 4);
        glDisableClientState(GL_VERTEX_ARRAY);
    }
    int hit(vertex_t *v1, vertex_t *v2, vertex_t *out_intersection, vertex_t *out_reflection)
    {
        static vertex_t va[4] = {-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
        vertex_t vp[4];
        int i;
        for(i = 0; i < 4; i++)
            vp[i] = vertex_mul(&va[i], &m);

        vertex_t intersect, nearest, normal;
        vertex_t ta = *v1;//unproject(v1);
        vertex_t tb = *v2;//unproject(v2);
        int hit = 0;
        float min = FLT_MAX;
        
        for(i = 0; i < 4; i++)
        {
            vertex_t la = vp[i];
            vertex_t lb = vp[(i+1)%4];
            vertex_t l = vertex_diff(&lb, &la);
            if(line_intersect(&ta, &tb, &la, &lb, &intersect))
            {
                //intersect = project(&intersect);
                float dist = vertex_distq(&ta, &intersect);
                if(dist < min)
                {
                    min = dist;
                    nearest = intersect;
                    normal = vertex_normal(&l);
                    
                    //nearest = vertex_normalize(&nearest);
                    normal = vertex_normalize(&normal);
                }
                hit++;
            }
        }        
        
        if(hit)
        {
            vertex_t vector_v = vertex_diff(&tb, &nearest);
            float dot_vn = vertex_dot(&vector_v, &normal);
            vertex_t v2n = (vertex_t){normal.x * dot_vn * 2.0f, normal.y * dot_vn * 2.0f};
            vertex_t vector_w = vertex_diff(&vector_v, &v2n);
            //vertex_t reflection = vertex_sum(&vector_w, &nearest);
            
            *out_reflection = vector_w;
            *out_intersection = nearest;
            return 1;
        }
        else
        {
            return 0;
        }
    }
    int update(Point2D *point, vertex_t *out_step)
    {
        int ret = 0;
        vertex_t step, step_p, pos_p, intersect, reflect;
        step = vertex_sum(&point->pos, &point->vel);
        step_p = unproject(&step);
        pos_p = unproject(&point->pos);
        int in1 = step_p.x > -1.0f && step_p.x < 1.0f && step_p.y > -1.0f && step_p.y < 1.0f;
        int in2 = pos_p.x > -1.0f && pos_p.x < 1.0f && pos_p.y > -1.0f && pos_p.y < 1.0f;
        if(in1 != in2)
        {
            vertex_t dir_n = vertex_normalize(&point->vel);
            vertex_t step2 = vertex_sum(&point->pos, &dir_n);
            int hits = hit(&point->pos, &step2, &intersect, &reflect);
            if(hits)
            {
                vertex_t reflect_n = vertex_normalize(&reflect);
                float norm = vertex_norm(&point->vel);
                point->vel = vertex_mul(&reflect_n, norm);
                //step = vertex_sum(&point->vel, &intersect);
                //step = intersect;
                step = point->pos;
                ret = 1;
            }
        }
        *out_step = step;
        return ret;
    }
};

static float video_w, video_h;
static int screen_w, screen_h;

#define NBOX 3
#define NPOINT 100

static vertex_t v[4];
static vertex_t touch, touch_start;
static Box2D boxes[NBOX];
static Point2D points[NPOINT];
int down;

void app_main()
{
    int i;
    touch = touch_start = (vertex_t){0};
    down = 0;
    for(i = 0; i < NPOINT; i++)
    {
        points[i] = Point2D();
        points[i].pos = (vertex_t){0};//{randfneg(), randfneg()};
        points[i].vel = (vertex_t){randfneg()*0.01f, randfneg() * 0.01f};
    }
}
void app_init(engine_video_t *video)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    screen_w = video->screen_width;
    screen_h = video->screen_height;
    float ratio = (float)screen_w / (float)screen_h;
    video_w = 1, video_h = video_w / ratio;
    glOrthof(-video_w, video_w, -video_h, video_h, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPointSize(4.0f);
}
void app_draw(int ms)
{
    static float theta = 0.0f;
    static int counter = 0;
    //if(counter++ < 5) return; else counter = 0;
    //matrix_t m, t, r, s;
    int i, j;
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    theta += 0.005f;
    
    boxes[0].identity();
    boxes[0].scale(video_w, video_h);
    boxes[0].draw();
    
    boxes[1].identity();
    boxes[1].scale(0.2f, 0.4f);
    //boxes[1].rotate(M_PI * 0.25f);
    boxes[1].translate(-0.5f, 0.0f);
    boxes[1].draw();
    
    boxes[2].identity();
    boxes[2].scale(0.1f, 0.1f);
    boxes[2].rotate(theta);
    boxes[2].translate(0.5f, 0.0f);
    boxes[2].draw();
    
    draw_point(&touch);
    draw_point(&touch_start);
    draw_line(&touch_start, &touch);
    
    if(down)
    {
        for(i = 0; i < NPOINT; i++)
            points[i].vel.y -= 0.001;
    }
    for(i = 0; i < NPOINT; i++)
        points[i].vel = vertex_mul(&points[i].vel, 0.99f);
    
//    vertex_t step, step_p, pos_p, intersect, reflect;
//    step = vertex_sum(&point.pos, &point.vel);
//    step_p = b.unproject(&step);
//    pos_p = b.unproject(&point.pos);
//    int in1 = step_p.x > -1.0f && step_p.x < 1.0f && step_p.y > -1.0f && step_p.y < 1.0f;
//    int in2 = pos_p.x > -1.0f && pos_p.x < 1.0f && pos_p.y > -1.0f && pos_p.y < 1.0f;
//    if(in1 != in2)
//    {
//        vertex_t dir_n = vertex_normalize(&point.vel);
//        vertex_t step2 = vertex_sum(&point.pos, &dir_n);
//        int hit = b.hit(&point.pos, &step2, &intersect, &reflect);
//        //if(!hit) hit = b.hit(&point.pos, &step2, &intersect, &reflect);
//        if(hit)
//        {
//            vertex_t reflect_n = vertex_normalize(&reflect);
//            float norm = vertex_norm(&point.vel);
//            point.vel = vertex_mul(&reflect_n, norm);
//            step = vertex_sum(&point.vel, &intersect);
//            //step = intersect;
//            step = point.pos;
//            
//            //draw_point(&intersect);
//            //draw_point(&reflect);
//            //draw_line(&intersect, &reflect);
//            //glColor4f(1, 0, 0, 1);
//            LOG("collision");
//        }
//        else 
//        {
//            glColor4f(0, 1, 0, 1);
//        }
//    }
//    point.pos = step;
    vertex_t step;
    for(j = 0; j < NPOINT; j++)
    {
        for(i = 0; i < NBOX; i++)
        {
            if(boxes[i].update(&points[j], &step))
            {
                break;
            }
        }
        points[j].pos = step;
        draw_point(&points[j].pos);
    }
}
void app_pause()
{
    
}
void app_destroy()
{
    engine_audio_destroy();
}
void app_touch_down(int x, int y, int tid)
{
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    touch = touch_start = (vertex_t){fx, fy};
    down = 1;
}
void app_touch_move(int x, int y, int tid)
{
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    touch = (vertex_t){fx, fy};
}
void app_touch_up(int x, int y, int tid)
{
    down = 0;
}
int app_key_down(int code)
{
    if(code == 4)
    {
        app_main();
        return 1;
    }
    return 0;
}
int app_key_up(int code)
{
    return 0;
}
