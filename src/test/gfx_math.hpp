#include <math.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Matrix Algebra
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct vertex_t
{
    float x, y;
} vertex_t;

typedef struct matrix_t
{
    float m11, m12, m13;
    float m21, m22, m23;
    float m31, m32, m33;
} matrix_t;

static matrix_t matrix_mul(matrix_t *a, matrix_t *b)
{
    matrix_t res;
    res.m11 = (a->m11 * b->m11) + (a->m12 * b->m21) + (a->m13 * b->m31);
    res.m12 = (a->m11 * b->m12) + (a->m12 * b->m22) + (a->m13 * b->m32);
    res.m13 = (a->m11 * b->m13) + (a->m12 * b->m23) + (a->m13 * b->m33);
    res.m21 = (a->m21 * b->m11) + (a->m22 * b->m21) + (a->m23 * b->m31);
    res.m22 = (a->m21 * b->m12) + (a->m22 * b->m22) + (a->m23 * b->m32);
    res.m23 = (a->m21 * b->m13) + (a->m22 * b->m23) + (a->m23 * b->m33);
    res.m31 = (a->m31 * b->m11) + (a->m32 * b->m21) + (a->m33 * b->m31);
    res.m32 = (a->m31 * b->m12) + (a->m32 * b->m22) + (a->m33 * b->m32);
    res.m33 = (a->m31 * b->m13) + (a->m32 * b->m23) + (a->m33 * b->m33);
    return res;
}

static vertex_t vertex_mul(vertex_t *a, matrix_t *b)
{
    vertex_t res;
    res.x = (a->x * b->m11) + (a->y * b->m21) + b->m31;
    res.y = (a->x * b->m12) + (a->y * b->m22) + b->m32;
    return res;
}

static vertex_t vertex_mul(vertex_t *v, float f)
{
    return (vertex_t){v->x * f, v->y * f};
}

static float vertex_dot(vertex_t *a, vertex_t *b)
{
    return a->x * b->x + a->y * b->y;
}

static vertex_t vertex_diff(vertex_t *a, vertex_t *b)
{
    return (vertex_t){a->x - b->x, a->y - b->y};
}

static vertex_t vertex_sum(vertex_t *a, vertex_t *b)
{
    return (vertex_t){a->x + b->x, a->y + b->y};
}

static vertex_t vertex_normal(vertex_t *a)
{
    return (vertex_t){-a->y, a->x};
}

static vertex_t vertex_normalize(vertex_t *a)
{
    float dist = 1.0f / sqrtf(a->x * a->x + a->y * a->y);
    return (vertex_t){a->x * dist, a->y * dist};
}

static float vertex_distq(vertex_t *a, vertex_t *b)
{
    float dx = a->x - b->x;
    float dy = a->y - b->y;
    return dx * dx + dy * dy;
}

static float vertex_dist(vertex_t *a, vertex_t *b)
{
    float dx = a->x - b->x;
    float dy = a->y - b->y;
    return sqrtf(dx * dx + dy * dy);
}

static float vertex_norm(vertex_t *a)
{
    return sqrtf(a->x * a->x + a->y * a->y);
}

static int line_intersect(vertex_t *a, vertex_t *b, vertex_t *c, vertex_t *d, vertex_t *out, float limit)
{
    vertex_t e = vertex_diff(b, a);
    vertex_t f = vertex_diff(d, c);
    vertex_t g = vertex_diff(a, c);
    vertex_t p = (vertex_t){-e.y, e.x};
    vertex_t q = (vertex_t){-f.y, f.x};
    float dot_fp = vertex_dot(&f, &p);
    if(dot_fp == 0.0f) return 0;
    float dot_gp = vertex_dot(&g, &p);
    float dot_gq = vertex_dot(&g, &q);
    float h = dot_gp / dot_fp;
    float t = dot_gq / dot_fp;
    out->x = c->x + f.x * h;
    out->y = c->y + f.y * h;
    float la = 1.0f - limit;
    float lb = limit;
    return (h <= la && h >= lb) && (t >= lb && t <= la);
}

static matrix_t matrix_zero()
{
    return (matrix_t){0};
}

static matrix_t matrix_identity()
{
    return (matrix_t){1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
}

static matrix_t matrix_transpose(matrix_t *m)
{
    matrix_t res;
    res.m11 = m->m11;
    res.m12 = m->m21;
    res.m13 = m->m31;
    res.m21 = m->m12;
    res.m22 = m->m22;
    res.m23 = m->m32;
    res.m31 = m->m13;
    res.m32 = m->m23;
    res.m33 = m->m33;
    return res;
}

static matrix_t matrix_invert(matrix_t *m)
{
    matrix_t res = *m;
    float sx = 1.0f / (m->m11 * m->m11 + m->m12 * m->m12);
    float sy = 1.0f / (m->m21 * m->m21 + m->m22 * m->m22);
    res.m11 = m->m11 * sx;
    res.m12 = m->m21 * sy;
    res.m21 = m->m12 * sx;
    res.m22 = m->m22 * sy;
    res.m31 = (-m->m31 * m->m11 - m->m32 * -m->m12) * sx;
    res.m32 = (-m->m31 * m->m21 - m->m32 * -m->m22) * sy;
    return res;
}

static matrix_t matrix_rotation(float theta)
{
    float s = sinf(theta);
    float c = cosf(theta);
    return (matrix_t){c, s, 0.0f, -s, c, 0.0f, 0.0f, 0.0f, 1.0f};
}

static matrix_t matrix_translation(float tx, float ty)
{
    return (matrix_t){1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, tx, ty, 1.0f};
}

static matrix_t matrix_scaling(float sx, float sy)
{
    return (matrix_t){sx, 0.0f, 0.0f, 0.0f, sy, 0.0f, 0.0f, 0.0f, 1.0f};
}

static inline void matrix_translate(matrix_t *m, float tx, float ty)
{
    m->m31 += tx;
    m->m32 += ty;
}

static inline void matrix_rotate(matrix_t *m, float theta)
{
    float s = sinf(theta);
    float c = cosf(theta);
    m->m11 = c;
    m->m12 = s;
    m->m21 = -s;
    m->m22 = c;
}
