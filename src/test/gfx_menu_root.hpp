////////////////////////////////////////////////////////////////////
class MenuSceneEmpty : public MenuScene {};
////////////////////////////////////////////////////////////////////
class MenuSceneInit : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_about, btn_play, btn_exit;
    gfxBitmapFont small;

    void init()
    {
        super::init();
        title = "Main Menu";
        init_button(&btn_play, "Play", EVENT_PLAY);
        init_button(&btn_about, "About", EVENT_ABOUT);
        distribute_v(2, 0.5f, -0.1f, &btn_play.pos, &btn_about.pos);
        //init_button(&btn_exit, "Exit", EVENT_EXIT);
        //btn_exit.pos.y = back_y;
        small = font;
        small.scale *= 0.7f;
    }

    void draw()
    {
        super::draw();
        draw_start();
        small.draw_text(
            "This is an alpha release.\n"
            "Please read the About to\n"
            "to understand what does it\n"
            "mean. Please report if you\n"
            "find any kind of bugs.\n"
            "Thanks and have fun :)\n", -0.9, -0.5);
        draw_end();
    }
};
////////////////////////////////////////////////////////////////////
class MenuScenePause : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_menu, btn_play, btn_restart;

    void init()
    {
        super::init();
        title = "Pause";
        init_button(&btn_play, "Continue", EVENT_BACK);
        init_button(&btn_menu, "Menu", EVENT_MENU);
        init_button(&btn_restart, "Restart", EVENT_RESTART);
        distribute_v(3, 0.5f, -0.5f, &btn_play.pos, &btn_restart, &btn_menu.pos);
    }
};
////////////////////////////////////////////////////////////////////
class MenuSceneAbout : public MenuScene
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_back;
    gfxBitmapFont small;

    void init()
    {
        super::init();
        title = "About";
        init_button(&btn_back, "Back", EVENT_BACK);
        btn_back.pos.y = back_y;
        small = font;
        small.scale *= 0.7f;
    }

    void draw()
    {
        super::draw();
        draw_start();
        small.draw_text(
            "Hi, thanks for downloading\n"
            "this game. As you can see\n"
            "it's not complete yet, but\n"
            "I'm working hard to improve\n"
            "in my free time.\n"
            "This game is totally free.\n"
            "It's part of my mobile game\n"
            "engine made with NDK and\n"
            "totally in C++ code.\n"
            "If you don't understand\n"
            "don't worry about it :)\n\n"
            "If you are a developer\n"
            "and want to know more\n"
            "about this engine you\n"
            "can send me an email at\n"
            "omarator@gmail.com\n\n"
            "Have fun!", -0.9, 0.9);
        draw_end();
    }
};
////////////////////////////////////////////////////////////////////
