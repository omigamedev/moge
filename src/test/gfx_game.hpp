//#define NBALLS 10

class Ball
{
public:
    gfxVector2D pos, vel, offset;
    int m_tid;
    int m_collide;
    float m_theta;
    float scale;

    Ball()
    {
        m_tid = -1;
        m_collide = 0;
        m_theta = 0.0f;
    }
};

class MenuSceneGame : public MenuScene, public gfxGuiObject
{
public:
    typedef MenuScene super;
    gfxGuiButton btn_credits, btn_play, btn_exit;
    gfxSpriteMultipart m_sprite;
    int m_time;
    int m_enabled;
    int m_nis;
    int m_end;
    int m_drag;
    int m_wait;
    int m_nballs;
    int m_level;
    char m_str_time[32];
    char m_str_level[64];
    Ball m_ball[50];

    MenuSceneGame()
    {
        m_time = 0;
        m_enabled = 0;
        m_nis = -1;
        m_end = 0;
        m_drag = 0;
        m_nballs = 10;
        m_level = 0;
    }

    void init()
    {
        m_time = 0;
        m_enabled = 0;
        m_nis = -1;
        m_end = 0;
        m_drag = 0;
        for(int i = 0; i < 50; i++)
        {
            m_ball[i] = Ball();
            m_ball[i].pos = gfxVector2D(randfneg()*(m_width-0.1f),
                randfneg()*(m_height-0.3f)-0.1f);
            m_ball[i].scale = 0.1f;
            m_ball[i].m_theta = randf() * M_PI * 2.0f;
        }
        resume();
    }

    void reset()
    {
        m_time = 0;
        m_enabled = 0;
        m_nis = -1;
        m_end = 0;
        m_drag = 0;
        for(int i = 0; i < m_nballs; i++)
        {
            m_ball[i].pos = gfxVector2D(randfneg()*(m_width-0.1f),
                randfneg()*(m_height-0.3f)-0.1f);
        }
    }

    void resume()
    {
        for(int i = 0; i < 50; i++)
        {
            //m_ball[i].set_texture(&m_tex_ball);
            //m_ball[i].set_region(150, 6, 70, 70);
        }
    }

    void destroy()
    {
        //m_tex_ball.unload();
    }

    void disable()
    {
        m_enabled = 0;
        unhandle_touch();
    }

    void enable()
    {
        m_enabled = 1;
        handle_touch();
    }

    int on_touch(T_GUI type, float x, float y, int id)
    {
        int ret = GFX_GUI_UNHANDLED;

        if(m_end) return ret;

        if(type == T_GUI_TOUCH_DOWN)
        {
            for(int i = 0; i < m_nballs; i++)
            {
                float dx = m_ball[i].pos.x - x;
                float dy = m_ball[i].pos.y - y;
                float dsq = dx * dx + dy * dy;
                float rsq = (m_ball[i].scale * m_ball[i].scale) * 2.0f;
                if(dsq < rsq)
                {
                    m_ball[i].m_tid = id;
                    //m_ball[i].pos = gfxVector2D(x, y);
                    m_ball[i].offset.x = x - m_ball[i].pos.x;
                    m_ball[i].offset.y = y - m_ball[i].pos.y;
                    m_drag = 1;
                    break;
                }
            }
        }
        else if(type == T_GUI_TOUCH_MOVE)
        {
            for(int i = 0; i < m_nballs; i++)
            {
                if(m_ball[i].m_tid == id)
                {
                    m_ball[i].pos.x = x - m_ball[i].offset.x;
                    m_ball[i].pos.y = y - m_ball[i].offset.y;
                }
            }
        }
        else if(type == T_GUI_TOUCH_UP)
        {
            for(int i = 0; i < m_nballs; i++)
            {
                if(m_ball[i].m_tid == id)
                {
                    m_ball[i].m_tid = -1;
                }
            }
            m_drag = 0;
        }
        return ret;
    }

    void update()
    {
        // test for game win
        if(m_nis == 0 && m_drag == 0 && m_end == 0)
        {
            m_end = 1;
            m_wait = 30;
        }
        // update balls
        for(int i = 0; i < m_nballs; i++)
        {
            m_ball[i].m_theta += 0.05f;
        }
        // update timer text
        int mm, ss, ms;
        int sec = m_time / 10;
        ms = m_time % 10;
        mm = sec / 60;
        ss = sec - mm * 60;
        sprintf(m_str_time, "%02d:%02d:%d", mm, ss, ms);
        // update level text
        sprintf(m_str_level, "level %d", m_level);
    }

    void draw()
    {
        if(m_end == 0)
        {
            update();
        }
        else
        {
            if(m_wait == 0)
            {
                delegate->on_scene_delegate(this, EVENT_WIN);
                m_wait = -1;
            }
            else
            {
                m_wait--;
            }
        }
        draw_start();
            draw_balls();
            draw_gui();
        draw_end();
    }

    void draw_balls()
    {
        vertex_t link[2], link2[2], out;
        int intersect;
        float rsq = m_ball[0].scale * m_ball[0].scale;

        m_nis = 0;
        // draw balls
        for(int i = 0; i < m_nballs; i++)
        {
            // draw links
            int a = i, b = (i + 1) % m_nballs;
            link[0].x = m_ball[a].pos.x;
            link[0].y = m_ball[a].pos.y;
            link[1].x = m_ball[b].pos.x;
            link[1].y = m_ball[b].pos.y;

            for(int j = 0; j < m_nballs; j++)
            {
                int a = j, b = (j + 1) % m_nballs;
                link2[0].x = m_ball[a].pos.x;
                link2[0].y = m_ball[a].pos.y;
                link2[1].x = m_ball[b].pos.x;
                link2[1].y = m_ball[b].pos.y;

                if(i != j && line_intersect(&link[0], &link[1], &link2[0], &link2[1], &out, 0.001f))
                {
                    intersect = 1;
                    m_nis++;
                    break;
                }
                else
                {
                    intersect = 0;
                }
            }

            if(m_ball[a].m_tid != -1 || m_ball[b].m_tid != -1 || intersect)
                glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            else
                glColor4f(1.0f, 1.0f, 1.0f, 0.3f);

            glPointSize(5.0f);
            glLineWidth(10.0f);

            glDisableClientState(GL_TEXTURE_COORD_ARRAY);
            glBindTexture(GL_TEXTURE_2D, (GLuint)0);
            if(intersect)
            {
                glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
                glVertexPointer(2, GL_FLOAT, 0, &out);
                glDrawArrays(GL_POINTS, 0, 1);
            }
            glVertexPointer(2, GL_FLOAT, 0, link);
            glDrawArrays(GL_LINES, 0, 2);
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);

            // draw ball sprite
            glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            m_sprite.pos = m_ball[i].pos;
            m_sprite.scale = m_ball[i].scale;
            m_sprite.draw();
        }

    }

    void draw_gui()
    {
        font.draw_text(m_str_time, -0.9, -m_height + 0.1f);
        font.draw_text_right(m_str_level, 0.9, -m_height + 0.1f);
        if(m_end) font.draw_text_center("You Win!", 0.0f, 0.0f);
    }

    void timerTick()
    {
        m_time++;
    }
};
