#include "engine.h"
#include "android_core.h"
#include <math.h>
#include <ivorbisfile.h>
#include <stdlib.h>

unsigned char pcmout[4096] = {0};
int snd;

void app_main()
{
}
void app_init(engine_video_t *video)
{
    FILE *fp;
    fp = engine_asset_open("music.ogg");
    
    OggVorbis_File vf;
    int res = ov_open(fp, &vf, NULL, 0);
    if (res < 0)
    {
        LOG("Input does not appear to be an Ogg bitstream.\n");
        exit(0);
    }
    else
    {
        // Throw the comments plus a few lines about the bitstream we're decoding
        char **ptr = ov_comment(&vf, -1)->user_comments;
        vorbis_info *vi = ov_info(&vf, -1);
        while (*ptr)
        {
            LOG("%s", *ptr);
            ++ptr;
        }
        LOG("Bitstream is %d channel, %ldHz", vi->channels, vi->rate);
        LOG("Decoded length: %ld samples", (long) ov_pcm_total(&vf, -1));
        LOG("Encoded by: %s", ov_comment(&vf, -1)->vendor);
    }

    LOG("init sound");
    engine_audio_init();
    engine_audio_volume_set(1.0f);
    
    // Get sample info
    vorbis_info* vi = ov_info(&vf, -1);
    unsigned int uiPCMSamples = (unsigned int)ov_pcm_total(&vf, -1);
    long len = uiPCMSamples * vi->channels * sizeof(short);
    void* pvPCMBuffer = malloc(4096*8);
    memset(pvPCMBuffer, 0, len);

    // create player
    android_sound_player_t pl;
    if(player_create(&pl, 8) == 0) LOG("player create error");

    // Decode!
    int current_section = 0;
    long iRead = 0;
    int count = 8;
    unsigned int len2 = 0;
    unsigned int uiCurrPos = 0;
    unsigned int localRead;
    int max = 50;
    do
    {
        localRead = 0;
        for(len2 = 0; len2 < 8; len2++)
        {
            iRead = ov_read(&vf, (char*)pvPCMBuffer + uiCurrPos, 4096, &current_section);
            uiCurrPos += (unsigned int)iRead;
            localRead += (unsigned int)iRead;
        }
        while(engine_queue_add(&pl, (char*)pvPCMBuffer + uiCurrPos-localRead, localRead) == 0);
    }
    while (iRead != 0 /*&& max-- > 0*/);
    
    LOG("done");

    // Cleanup
    ov_clear(&vf);
    
    //snd = engine_audio_sound_buffer(pvPCMBuffer, len);
    //LOG("len=%d", len);
    
/*
    LOG("load ogg");
    int current_section;
    long ret = ov_read(&vf, pcmout, sizeof(pcmout), &current_section);
    //ret = ov_read(&vf, pcmout, sizeof(pcmout), &current_section);
    if (ret == 0) 
    {
        LOG("EOF");
    } 
    else if(ret < 0) 
    {
        LOG("stream error");
    } 
    else 
    {
        LOG("stream decoded current_section=%d", current_section);
        snd = engine_audio_sound_buffer(pcmout, sizeof(pcmout));
    }
    
    snd = engine_audio_sound_load("splat.raw");
*/
}
void app_draw(int ms)
{
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}
void app_pause()
{
}
void app_destroy()
{
    engine_audio_destroy();
}
void app_touch_down(int x, int y, int tid)
{
    LOG("touch");
    //engine_audio_sound_play(snd, 1.0f);
}
void app_touch_move(int x, int y, int tid)
{
}
void app_touch_up(int x, int y, int tid)
{
}
int app_key_down(int code)
{
    return 0;
}
int app_key_up(int code)
{
    return 0;
}
