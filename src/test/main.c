#include "engine.h"

#ifdef ENGINE_IOS
#   define MUSIC "music.caf"
#elif ENGINE_ANDROID
#   define MUSIC "music.ogg"
#endif

static int frame = 0, blast, tid;
static clock_t tstart;

#define NS 10
static float history[NS][10][2];
static int history_index[NS];
static float positions[NS][2];
//static float colours[NS][4];
static float sizes[NS];
static int ids[NS];
static int sprite_index[NS];

static float video_w, video_h;
static int screen_w, screen_h;

typedef struct sprite_t
{
    GLuint tid;
    float x;
    float y;
    float sx;
    float sy;
    float velx;
    float vely;
    float vertex[8];
    float tex[8];
} sprite_t;

static sprite_t sprites[NS];
static int sprites_count;

static int sprite_get()
{
    int ret = sprites_count;
    sprites_count++;
    sprites[ret].tex[0] = 0.0f;
    sprites[ret].tex[1] = 1.0f;
    sprites[ret].tex[2] = 1.0f;
    sprites[ret].tex[3] = 1.0f;
    sprites[ret].tex[4] = 1.0f;
    sprites[ret].tex[5] = 0.0f;
    sprites[ret].tex[6] = 0.0f;
    sprites[ret].tex[7] = 0.0f;
//    sprites[ret].vertex[0] = -1.0f;
//    sprites[ret].vertex[1] = -1.0f;
//    sprites[ret].vertex[2] = 1.0f;
//    sprites[ret].vertex[3] = -1.0f;
//    sprites[ret].vertex[4] = 1.0f;
//    sprites[ret].vertex[5] = 1.0f;
//    sprites[ret].vertex[6] = -1.0f;
//    sprites[ret].vertex[7] = 1.0f;
    return ret;
}

static void sprite_remove(int index)
{
    if(index < sprites_count)
    {
        sprites[index] = sprites[sprites_count];
    }
    sprites_count--;
}

static void sprite_update()
{
    int i;
    for(i = 0; i < sprites_count; i++)
    {
        sprites[i].sx = 0.3f;
        sprites[i].sy = 0.3f;
        sprites[i].x += sprites[i].velx;
        sprites[i].y += sprites[i].vely;
//        sprites[i].tex[0] = 0.0f;
//        sprites[i].tex[1] = 1.0f;
//        sprites[i].tex[2] = 1.0f;
//        sprites[i].tex[3] = 1.0f;
//        sprites[i].tex[4] = 1.0f;
//        sprites[i].tex[5] = 0.0f;
//        sprites[i].tex[6] = 0.0f;
//        sprites[i].tex[7] = 0.0f;
        sprites[i].vertex[0] = -1.0f * sprites[i].sx + sprites[i].x;
        sprites[i].vertex[1] = -1.0f * sprites[i].sy + sprites[i].y;
        sprites[i].vertex[2] = 1.0f * sprites[i].sx + sprites[i].x;
        sprites[i].vertex[3] = -1.0f * sprites[i].sy + sprites[i].y;
        sprites[i].vertex[4] = 1.0f * sprites[i].sx + sprites[i].x;
        sprites[i].vertex[5] = 1.0f * sprites[i].sy + sprites[i].y;
        sprites[i].vertex[6] = -1.0f * sprites[i].sx + sprites[i].x;
        sprites[i].vertex[7] = 1.0f * sprites[i].sy + sprites[i].y;
    }
}

static void sprite_draw()
{
    int i;
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    for(i = 0; i < sprites_count; i++)
    {
        glVertexPointer(2, GL_FLOAT, 0, sprites[i].vertex);
        glTexCoordPointer(2, GL_FLOAT, 0, sprites[i].tex);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    }
    
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}

void app_main()
{
    int i;
    sprites_count = 0;
    memset(sprites, 0, sizeof(sprites));
    memset(history, 0, sizeof(history));
    memset(history_index, 0, sizeof(history_index));
    memset(positions, 0, sizeof(positions));
    memset(sizes, 0, sizeof(sizes));
    for(i = 0; i < NS; i++) ids[i] = -1;
}
void app_init(engine_video_t *video)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    screen_w = video->screen_width;
    screen_h = video->screen_height;
    float ratio = (float)screen_w / (float)screen_h;
    video_w = 1, video_h = video_w / ratio;
    glOrthof(-video_w, video_w, -video_h, video_h, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    //glEnable(GL_POINT_SPRITE_OES);
    
    glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glPointSize(60.0f);
    
    LOG("init video %dx%d", video->screen_width, video->screen_height);
    FILE *fp = engine_asset_open("shot.png");
    if(fp == NULL) LOG("error");
    else LOG("ok file");
    
    int w, h;
    tid = engine_texture_load_png(fp, &w, &h);
    if(tid == 0) LOG("error loading texture");
    else LOG("texture %dx%d", w, h);
    
    //engine_audio_init();
    //engine_audio_music_load(MUSIC);
    //engine_audio_music_play(0, TRUE);
    
    //blast = engine_audio_sound_load("blast.raw");
    
    sprite_get();
    sprite_get();
    
    tstart = engine_time_getms();
}
void app_draw(float dt)
{
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
    glBindTexture(GL_TEXTURE_2D, tid);
    //int sourceRect[4] = {0, 0, 128, 128};
    //glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_CROP_RECT_OES, sourceRect);
    
    sprites[0].x = positions[0][0];
    sprites[0].y = positions[0][1];
    
    sprite_update();
    sprite_draw();
    
    clock_t tstop = engine_time_getms();
    if (tstop - tstart > 1000)
    {
        tstart = tstop;
        //LOG("clock frames %d", frame);
        frame = 0;
    }
    frame++;
}
void app_pause()
{
    
}
void app_destroy()
{
    engine_audio_destroy();
}
void app_touch_down(int x, int y, int tid)
{
    int i;
    //LOG("down %d", tid);
    engine_audio_sound_play(blast, 0.1f);
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    for(i = 0; i < NS; i++)
    {
        if(ids[i] == -1)
        {
            ids[i] = tid;
            positions[i][0] = fx;
            positions[i][1] = fy;
            sizes[i] = 60.0f;
            history_index[i] = 1;
            history[i][0][0] = fx;
            history[i][0][1] = fy;
            break;
        }
    }
}
void app_touch_move(int x, int y, int tid)
{
    int i;
    //LOG("move %d", tid);
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    for(i = 0; i < NS; i++)
    {
        if(ids[i] == tid)
        {
            positions[i][0] = fx;
            positions[i][1] = fy;
            history[i][history_index[i]][0] = fx;
            history[i][history_index[i]][1] = fy;
            history_index[i] = (history_index[i] + 1) % 10;
            break;
        }
    }
}
void app_touch_up(int x, int y, int tid)
{
    int i;
    //LOG("up %d", tid);
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    for(i = 0; i < NS; i++)
    {
        if(ids[i] == tid)
        {
            ids[i] = -1;
            positions[i][0] = fx;
            positions[i][1] = fy;
            sizes[i] = 0.0f;
            history[i][history_index[i]][0] = fx;
            history[i][history_index[i]][1] = fy;
            history_index[i] = (history_index[i] + 1) % 10;
            break;
        }
    }
}
void app_key_down(int code)
{
    LOG("key %d", code);
}
void app_key_up(int code)
{
    LOG("key %d", code);
}
