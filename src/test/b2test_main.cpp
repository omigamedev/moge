#include "engine.h"
#include <math.h>
#include <vector>
#include <Box2D/Box2D.h>

static float video_w, video_h;
static int screen_w, screen_h;
static float rad2deg = 180.0f / b2_pi;
static float deg2rad = b2_pi / 180.0f;
static float gl2box = 10.0f / 1.0f;
static float box2gl = 1.0f / 10.0f;

class Brick
{
public:
    b2Body *m_body;
    void init(b2World *world, const b2Vec2 &pos, const b2Vec2 &size)
    {
        b2BodyDef bdef;
        bdef.position = pos;
        m_body = world->CreateBody(&bdef);
        b2PolygonShape bshape;
        bshape.SetAsBox(size.x, size.y);
        m_body->CreateFixture(&bshape, 1.0f);
    }
    void draw()
    {
        b2Vec2 pos = box2gl * m_body->GetPosition();
        float angle = m_body->GetAngle() * rad2deg;
        b2Fixture *flist = m_body->GetFixtureList();
        b2PolygonShape *shape = (b2PolygonShape*)flist[0].GetShape();
        
        glBindTexture(GL_TEXTURE_2D, 0);
        
        glPushMatrix();
        glEnableClientState(GL_VERTEX_ARRAY);
        
        glTranslatef(pos.x, pos.y, 0.0f);
        glRotatef(angle, 0.0f, 0.0f, 1.0f);
        glScalef(box2gl, box2gl, box2gl);
        glVertexPointer(2, GL_FLOAT, 0, shape->m_vertices);
        
        glPointSize(4.0f);
        glDrawArrays(GL_LINE_LOOP, 0, shape->m_vertexCount);
        glDrawArrays(GL_POINTS, 0, shape->m_vertexCount);
        
        glDisableClientState(GL_VERTEX_ARRAY);
        glPopMatrix();        
    }
};

class Input
{
public:
    virtual void go() = 0;
};

class Ball : public Input
{
public:
    b2Body *m_body;
    b2Vec2 m_vertex[16];
    void init(b2World *world, const b2Vec2 &pos, float radius)
    {
        b2BodyDef bdef;
        bdef.position = pos;
        bdef.type = b2_dynamicBody;
        m_body = world->CreateBody(&bdef);
        b2CircleShape bshape;
        bshape.m_radius = radius;
        b2FixtureDef fdef;
        fdef.density = 1.0f;
        fdef.restitution = 0.3f;
        fdef.shape = &bshape;
        m_body->CreateFixture(&fdef);
        
        float theta_step = b2_pi / 8.0f;;
        for(int i = 0; i < 16; i++)
        {
            m_vertex[i] = radius * b2Vec2(cosf(theta_step * i), sinf(theta_step * i));
        }
    }
    void draw()
    {
        b2Vec2 pos = box2gl * m_body->GetPosition();
        float angle = m_body->GetAngle() * rad2deg;
        
        glPushMatrix();
        glEnableClientState(GL_VERTEX_ARRAY);
        
        glTranslatef(pos.x, pos.y, 0.0f);
        glRotatef(angle, 0.0f, 0.0f, 1.0f);
        glScalef(box2gl, box2gl, box2gl);
        glVertexPointer(2, GL_FLOAT, 0, m_vertex);
        
        glPointSize(4.0f);
        glDrawArrays(GL_LINE_LOOP, 0, 16);
        glDrawArrays(GL_POINTS, 0, 16);
        
        glDisableClientState(GL_VERTEX_ARRAY);
        glPopMatrix();        
    }
    void go()
    {

    }
};

static b2World *world;
static b2Body *ground, *box;
static GLuint tid_splat, tid_floor, tid_ball;
static int down = 0;
static Brick bricks[10];
static Ball ball;

static float randf()
{
    static float div = 1.0f / (float) RAND_MAX;
    return (float) (rand() % RAND_MAX) * div;
}
static void body_draw(b2Body *body, GLuint tid)
{
    static float triangleTex[8] = {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};
    b2Vec2 pos =  box2gl * body->GetPosition();
    float angle = body->GetAngle() * rad2deg;
    b2Fixture *flist = body->GetFixtureList();
    b2PolygonShape *shape = (b2PolygonShape*)flist[0].GetShape();
    
    glBindTexture(GL_TEXTURE_2D, tid);
    
    glPushMatrix();
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glTranslatef(pos.x, pos.y, 0.0f);
    glRotatef(angle, 0.0f, 0.0f, 1.0f);
    glScalef(box2gl, box2gl, box2gl);
    glVertexPointer(2, GL_FLOAT, 0, shape->m_vertices);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);
    
    glPointSize(4.0f);
    glDrawArrays(GL_LINE_LOOP, 0, shape->m_vertexCount);
    glDrawArrays(GL_POINTS, 0, shape->m_vertexCount);
    glDrawArrays(GL_TRIANGLE_FAN, 0, shape->m_vertexCount);
    
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glPopMatrix();
}
void app_main()
{
}
void app_init(engine_video_t *video)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    screen_w = video->screen_width;
    screen_h = video->screen_height;
    float ratio = (float)screen_w / (float)screen_h;
    video_w = 1, video_h = video_w / ratio;
    glOrthof(-video_w, video_w, -video_h, video_h, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    
    glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    world = new b2World(b2Vec2(0.0f, -40.0f));
    
    // define borders
    b2BodyDef groundef;
    b2PolygonShape groundshape;
    groundef.position.Set((10.0f-video_w) * gl2box, -video_h * gl2box);
    ground = world->CreateBody(&groundef);
    groundshape.SetAsBox(10.0f * gl2box, 0.1f * gl2box);
    ground->CreateFixture(&groundshape, 0.0f);
    
    //b.init(world, b2Vec2(0.0f, 0.0f), b2Vec2(1.0f, 1.0f));
    ball.init(world, b2Vec2(), 1.0f);
    
    for(int i = 0; i < 10; i++)
    {
        bricks[i].init(world, b2Vec2(10.0f * i, (-video_h + 0.2f) * gl2box), b2Vec2(1.0f, 1.0f));
    }
    
    //b2BodyDef boxdef;
    //boxdef.position.Set(0.0f, 0.0f);
    //boxdef.type = b2_dynamicBody;
    //box = world->CreateBody(&boxdef);
    //b2PolygonShape boxshape;
    //boxshape.SetAsBox(1.0f, 1.0f);
    //b2FixtureDef boxfixture;
    //boxfixture.shape = &boxshape;
    //boxfixture.density = 1.0f;
    //boxfixture.friction = 0.5f;
    //box->CreateFixture(&boxfixture);
    
    int w, h;
    FILE *fp;
    fp = engine_asset_open("splat.png");
    tid_splat = engine_texture_load_png(fp, &w, &h);
    fp = engine_asset_open("ball.png");
    tid_ball = engine_texture_load_png(fp, &w, &h);
    fp = engine_asset_open("floor.png");
    tid_floor = engine_texture_load_png(fp, &w, &h);
}
void app_draw(int ms)
{
    static float time_step = 1.0f / 30.0f;
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
    //if(down) ball.m_body->ApplyLinearImpulse(b2Vec2(0.0f, 10.0f), ball.m_body->GetWorldPoint(b2Vec2(1.0f, 2.0f)));
    if(down) 
    {
        ball.m_body->ApplyLinearImpulse(b2Vec2(10.0f, 30.0f), ball.m_body->GetWorldCenter());
        down = false;
    }

    world->Step(time_step, 6, 2);
    
    b2Vec2 pos = ball.m_body->GetPosition();
    glTranslatef(-pos.x * box2gl, 0.0f, 0.0f);
    
    //body_draw(box);
    body_draw(ground, tid_floor);
    for(int i = 0; i < 10; i++)
    {
        bricks[i].draw();
    }
    //b.draw();
    ball.draw();
}
void app_pause()
{
    
}
void app_destroy()
{
}
void app_touch_down(int x, int y, int tid)
{
    down = 1;
}
void app_touch_move(int x, int y, int tid)
{
}
void app_touch_up(int x, int y, int tid)
{
    down = 0;
}
int app_key_down(int code)
{
    return 0;
}
int app_key_up(int code)
{
    return 0;
}
void app_accelerate(float x, float y, float z)
{
    //world->SetGravity(b2Vec2(-x*10.0f, -y*10.0f));
    ball.m_body->ApplyLinearImpulse(b2Vec2(-x * 10.0, 0.0f), ball.m_body->GetWorldCenter());
}
