#include "engine.h"
#include <math.h>

#ifdef ENGINE_IOS
#   define MUSIC "music.caf"
#elif ENGINE_ANDROID
#   define MUSIC "music.ogg"
#endif

#define NS 1000
typedef struct shot_t
{
    float x, y;
    float r, g, b;
    float size;
    float rot;
    float velx, vely;
    unsigned char hit;
} ball_t;

static ball_t balls[NS];
static int balls_count;
static float increse;
static GLuint tid, tid2, tfloor;
static int tfloorw, tfloorh;
static int blast;

static float video_w, video_h;
static int screen_w, screen_h;

static float triangleCoords[8] = {-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
static float triangleTex[8] = {0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f};

static float randf()
{
    static float div = 1.0f / (float) RAND_MAX;
    return (float) (rand() % RAND_MAX) * div;
}
static void sprite_draw(float x, float y, float sx, float sy, float rot, GLuint tid)
{
    glBindTexture(GL_TEXTURE_2D, tid);
    
    glPushMatrix();
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    glTranslatef(x, y, 0.0f);
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
    glScalef(sx, sy, 1.0f);
    glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glPopMatrix();
}
static void sprite_gen()
{
    float angle = randf() * M_PI * 2.0f;
    float speed = randf() * 0.2f + 0.05f + increse;
    float x = cosf(angle) * 3.0f;
    float y = sinf(angle) * 3.0f;
    float velx = -x * speed;
    float vely = -y * speed;
    balls[balls_count].x = x;
    balls[balls_count].y = y;
    balls[balls_count].r = randf();
    balls[balls_count].g = randf();
    balls[balls_count].b = randf();
    balls[balls_count].velx = velx;
    balls[balls_count].vely = vely;
    balls[balls_count].size = 0.1f;//randf() * 0.5f + 0.2f;
    balls[balls_count].rot = randf() * 360.0f;
    balls_count++;
    increse += 0.01f;
}
void app_main()
{
    balls_count = 0;
    increse = 0.0f;
    memset(balls, 0, sizeof(balls));
}
GLint fboID;
GLuint defaultFrameBuffer, defaultRenderBuffer;
GLuint shadowFrameBuffer;
GLuint renderBuffer;
void app_init(engine_video_t *video)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    screen_w = video->screen_width;
    screen_h = video->screen_height;
    float ratio = (float)screen_w / (float)screen_h;
    video_w = 1, video_h = video_w / ratio;
    glOrthof(-video_w, video_w, -video_h, video_h, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    //glEnable(GL_POINT_SPRITE_OES);
    glDisable(GL_DEPTH_TEST);
    
    glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glPointSize(150.0f);
    
    LOG("init video %dx%d", video->screen_width, video->screen_height);
    int w, h;
    FILE *fp;
    fp = engine_asset_open("splat.png");
    tid = engine_texture_load_png(fp, &w, &h);
    fp = engine_asset_open("ball.png");
    tid2 = engine_texture_load_png(fp, &w, &h);
    fp = engine_asset_open("floor.png");
    tfloor = engine_texture_load_png(fp, &tfloorw, &tfloorh);
    
    engine_audio_init();
    engine_audio_volume_set(1.0f);
    if(engine_audio_music_load(MUSIC) == 1)
    {
        engine_audio_music_play(0, TRUE, 0.5f);
    }
    blast = engine_audio_sound_load("splat.raw");
    
    int i;
    for(i = 0; i < 4; i++)
        sprite_gen();
    
//    // get the current framebuffer and set as default
//    glGetIntegerv(GL_FRAMEBUFFER_BINDING, (GLint*)&defaultFrameBuffer);
//    glGetIntegerv(GL_RENDERBUFFER_BINDING, (GLint*)&defaultRenderBuffer);
//    
//    // Create a render buffer
//    glGenRenderbuffers(1, &renderBuffer);
//    glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
//    
//    // create a framebuffer object
//    glGenFramebuffers(1, &shadowFrameBuffer);
//    glBindFramebuffer(GL_FRAMEBUFFER, shadowFrameBuffer);
//    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);
//    
//    // Attach the RGB texture to FBO color attachment point
//    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tfloor, 0);
//    
//    glBindRenderbuffer(GL_RENDERBUFFER, defaultRenderBuffer);
//    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
}
void app_draw(int ms)
{
    static int count = 0.0f;
    static int frames = 0;
    count += ms;
    frames++;
    if(count > 1000)
    {
        //LOG("tick %d frames", frames);
        count = 0;
        frames = 0;
    }
    
    float dt = ms * 0.001f;
    
//    glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
//    glBindFramebuffer(GL_FRAMEBUFFER, shadowFrameBuffer);
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    sprite_draw(0.0f, 0.0f, video_h, video_h, 0.0f, tfloor);
    
    int i;
    for(i = 0; i < balls_count; i++)
    {
        if(balls[i].hit)
        {
            glColor4f(balls[i].r, balls[i].g, balls[i].b, 0.8f);
            sprite_draw(balls[i].x, balls[i].y, balls[i].size, balls[i].size, balls[i].rot, tid);
        }
    }
    
    //glBindTexture(GL_TEXTURE_2D, tfloor);
    //glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, tfloorw, tfloorh, 0);
    
//    glBindRenderbuffer(GL_RENDERBUFFER, defaultRenderBuffer);
//    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);
    
    
    for(i = 0; i < balls_count; i++)
    {
        if(!balls[i].hit)
        {
            glColor4f(balls[i].r, balls[i].g, balls[i].b, 1.0f);
            balls[i].x += balls[i].velx * dt;
            balls[i].y += balls[i].vely * dt;
            if(balls[i].x > video_w-balls[i].size){ balls[i].x = video_w-balls[i].size; balls[i].velx *= -1.0f; }
            if(balls[i].x < -video_w+balls[i].size){ balls[i].x = -video_w+balls[i].size; balls[i].velx *= -1.0f; }
            if(balls[i].y > video_h-balls[i].size){ balls[i].y = video_h-balls[i].size; balls[i].vely *= -1.0f; }
            if(balls[i].y < -video_h+balls[i].size){ balls[i].y = -video_h+balls[i].size; balls[i].vely *= -1.0f; }
            sprite_draw(balls[i].x, balls[i].y, balls[i].size, balls[i].size, balls[i].rot, tid2);            
        }
    }
//    sprite_draw(0.0f, 0.0f, 1.0f, 0.0f, tid);
}
void app_pause()
{
    
}
void app_destroy()
{
    engine_audio_destroy();
}
void app_touch_down(int x, int y, int tid)
{
    float fx = (float)x / (float)screen_w * 2.0f * video_w - video_w;
    float fy = -((float)y / (float)screen_h * 2.0f * video_h - video_h);
    int i;
    for(i = 0; i < balls_count; i++)
    {
        if(!balls[i].hit)
        {
            float dx = fx - balls[i].x;
            float dy = fy - balls[i].y;
            float dq = dx * dx + dy * dy;
            float rq = balls[i].size * balls[i].size * 4.0f;
            if(dq < rq)
            {
                balls[i].hit = 1;
                balls[i].size *= 4.0f;
                engine_audio_sound_play(blast, 1.0f);
                sprite_gen();
                break;
            }
        }
    }
}
void app_touch_move(int x, int y, int tid)
{
}
void app_touch_up(int x, int y, int tid)
{
}
int app_key_down(int code)
{
    LOG("key down %d", code);
    //if(code == 24) engine_audio_volume_offset(0.1f);
    //if(code == 25) engine_audio_volume_offset(-0.1f);
    if(code == 4 && balls_count > 4)
    {
        balls_count = 0;
        increse = 0.0f;
        memset(balls, 0, sizeof(balls));
        int i;
        for(i = 0; i < 4; i++)
            sprite_gen();
        return 1;
    }
    return 0;
}
int app_key_up(int code)
{
    LOG("key up %d", code);
    return 0;
}
