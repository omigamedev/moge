#include "../engine.h"
#include "../engine_gui.h"
#include "../engine_scene.h"
#include "gfx_math.hpp"
#include "gfx_menu_base.hpp"
#include "gfx_menu_root.hpp"
#include "gfx_game.hpp"
#include "gfx_app.hpp"

float MenuScene::m_height;
float MenuScene::m_width;
float MenuScene::client_top;
float MenuScene::client_bottom;
float MenuScene::title_y;
float MenuScene::back_y;
gfxBitmapFont MenuScene::font;

Main *Main::s = NULL;

void app_main()
{
    LOG("main");
    if(Main::s == NULL)
    {
        Main::s = new Main();
    }
    else
    {
        LOG("resume");
    }
    LOG("sizeof(gfxVector2D)=%d", sizeof(gfxVector2D));
}
void app_init(engine_video_t *video)
{
    float ratio = (float)video->screen_width / (float)video->screen_height;
    float ww = 1.0f;
    float hh = ww / ratio;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(-ww, ww, -hh, hh, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);

    for(int i = 0; i < 0xFFFF; i++) glDisable(i);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glClearColor(0.07f, 0.48f, 1.0f, 1.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    Main::s->onInit(ww, hh, video->screen_width, video->screen_height);
}
void app_draw(int ms)
{
    Main::s->onDraw(ms);
}
void app_pause()
{
    Main::s->onPause();
}
void app_destroy()
{
    Main::s->onDestroy();
    delete Main::s;
    Main::s = NULL;
}
void app_touch_down(int x, int y, int tid)
{
    Main::s->onTouchDown(x, y, tid);
}
void app_touch_move(int x, int y, int tid)
{
    Main::s->onTouchMove(x, y, tid);
}
void app_touch_up(int x, int y, int tid)
{
    Main::s->onTouchUp(x, y, tid);
}
int app_key_down(int code)
{
    return Main::s->onKeyDown(code);
}
int app_key_up(int code)
{
    return Main::s->onKeyUp(code);
}
