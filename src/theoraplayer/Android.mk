LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libtheoraplayer

LOCAL_SRC_FILES := \
src/Theora/TheoraVideoClip_Theora.cpp \
src/TheoraAsync.cpp \
src/TheoraAudioInterface.cpp \
src/TheoraAudioPacketQueue.cpp \
src/TheoraDataSource.cpp \
src/TheoraException.cpp \
src/TheoraFrameQueue.cpp \
src/TheoraTimer.cpp \
src/TheoraUtil.cpp \
src/TheoraVideoClip.cpp \
src/TheoraVideoFrame.cpp \
src/TheoraVideoManager.cpp \
src/TheoraWorkerThread.cpp \
src/YUV/C/yuv420_grey_c.c \
src/YUV/C/yuv420_rgb_c.c \
src/YUV/C/yuv420_yuv_c.c \
src/YUV/C/yuv_c.c

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
LOCAL_C_INCLUDES := $(LOCAL_PATH)/include/theoraplayer
LOCAL_CFLAGS := -D__THEORA -D_YUV_C -I$(LOCAL_PATH)/src -I$(LOCAL_PATH)/src/Theora -fexceptions
LOCAL_STATIC_LIBRARIES := ogg vorbis theora
                                 
include $(BUILD_STATIC_LIBRARY)

