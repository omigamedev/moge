LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := tremolo_static
LOCAL_MODULE_FILENAME := libtremolo

LOCAL_SRC_FILES := \
annotate.c \
bitwise.c \
codebook.c \
dsp.c \
floor0.c \
floor1.c \
floor_lookup.c \
framing.c \
info.c \
mapping0.c \
mdct.c \
misc.c \
profile.c \
res012.c \
testtremor.c \
vorbisfile.c \
bitwiseARM.s \
dpen.s \
floor1ARM.s \
floor1LARM.s \
mdctARM.s \
mdctLARM.s \
speed.s

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/..
LOCAL_C_INCLUDES := $(LOCAL_PATH)/..
LOCAL_CFLAGS := -D_ARM_ASSEM_
                                 
include $(BUILD_STATIC_LIBRARY)

