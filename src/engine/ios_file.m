#import "engine.h"

FILE* engine_asset_open(const char *filename)
{
    NSString *file = [NSString stringWithCString:filename encoding:NSUTF8StringEncoding];
    const char *path = [[[NSBundle mainBundle] pathForResource:file ofType:nil] cStringUsingEncoding:NSUTF8StringEncoding];
    return fopen(path, "rb");
}
