#ifndef _GFX_PARTICLE_
#define _GFX_PARTICLE_

#include "engine_3dmath.h"
#include "engine_texture.h"
#include <stdlib.h>
#include <math.h>

template <int N>
class gfxEmitter : public gfxObject
{
public:
	gfxVector2D _pos[N];
	gfxVector2D _vel[N];
	gfxColor _color[N];
	gfxColor _color_preset[4];
	int _life[N];
	float inertia;
	float angle;
	int life;
	
	int count;
	int count_color;
	int index;
	int index_color;
	int rate;
	int duration;
	
	gfxEmitter();
	
	void draw();
	void generate();
	void update();
};
template <int N>
gfxEmitter<N>::gfxEmitter()
{
	memset(_life, 0, sizeof(_life));
	memset(_color, 0, sizeof(_color));
	inertia = 1.0f;
	angle = 0.0f;
	life = 0;
	count = 0;
	index = 0;
	rate = 0;
	index_color = 0;
	count_color = 4;
	_color_preset[0] = gfxColor(141, 91, 17, 255);
	_color_preset[1] = gfxColor(210, 163, 91, 255);
	_color_preset[2] = gfxColor(85, 75, 39, 255);
	_color_preset[3] = gfxColor(34, 28, 4, 255);
	_color_preset[3] = gfxColor(117, 163, 189, 255);
}
template <int N>
void gfxEmitter<N>::generate()
{
	if(duration < 1) return;
	duration--;
	for(int i = 0; i < rate; i++)
	{
		angle = (float)(rand() % 1000) * 0.001f * 2.0f * M_PI;
		float vel = (float)(rand() % 1000) * 0.001f;
		float vx = sinf(angle) * 0.1f;
		float vy = cosf(angle) * 0.1f;
		_vel[index].x = vx * vel;
		_vel[index].y = vy * vel;
		_color[index] = _color_preset[index_color];
		_pos[index].x = pos.x + vx * 1.0f;
		_pos[index].y = pos.y + vy * 1.0f;
		_life[index] = life;
		index = (index+1) % N;
		index_color = (index_color+1) % count_color;
		count = count < N ? count + 1 : N ;
	}
}
template <int N>
void gfxEmitter<N>::draw()
{
	if(count == 0) return;
	glBindTexture(GL_TEXTURE_2D, 0);
	
	//glBindTexture(GL_TEXTURE_2D, (GLuint)tex.id);
	//glEnable(GL_POINT_SPRITE_OES);
	//glTexEnvi(GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE);
	
	//	int sourceRect[4] = {0, 24, 8, 16};
	//	glTexParameteriv(GL_TEXTURE_2D, GL_TEXTURE_CROP_RECT_OES, sourceRect);
	
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, _pos);
	glColorPointer(4, GL_FLOAT, 0, _color);
	glPointSize(1.5f);
	
	glDrawArrays(GL_POINTS, 0, N);
	
	glDisableClientState(GL_COLOR_ARRAY);
}
template <int N>
void gfxEmitter<N>::update()
{
	if(count == 0) return;
	for(int i = 0; i < N; i++)
	{
		if(_life[i] > 0)
		{
			_life[i]--;
			if(_life[i] == 0)
			{
				count--;
				_color[i].a = 0.0f;
				if(index > i)
				{
//					index--;
//					_life[i] = _life[index];
//					_pos[i] = _pos[index];
//					_color[i] = _color[index];
//					_vel[i] = _vel[index];
//					_life[index] = 0;
				}
			}
			else
			{
				_pos[i].x += _vel[i].x;
				_pos[i].y += _vel[i].y;
				//_alpha[i] = (float)_life[i] / (float)life;
//				if(_pos[i].x >  1.0f){_pos[i].x =  1.0f; _vel[i].x *= -1.0f;}
//				if(_pos[i].x < -1.0f){_pos[i].x = -1.0f; _vel[i].x *= -1.0f;}
//				if(_pos[i].y >  1.5f){_pos[i].y =  1.0f; _vel[i].y *= -1.5f;}
//				if(_pos[i].y < -1.5f){_pos[i].y = -1.0f; _vel[i].y *= -1.5f;}
			}
		}
	}
}
#endif
