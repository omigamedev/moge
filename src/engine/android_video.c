#include "android_core.h"

const EGLint video_attribs[] =
{
    EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
    EGL_BLUE_SIZE, 8,
    EGL_GREEN_SIZE, 8,
    EGL_RED_SIZE, 8,
    EGL_NONE
};
static EGLint video_w, video_h, video_format;
static EGLint video_nconfigs;
static EGLConfig video_config       = NULL;
static EGLSurface video_surface     = NULL;
static EGLContext video_context     = NULL;
static EGLDisplay video_display     = NULL;
static ANativeWindow *video_wnd     = NULL;
static int video_rendering          = FALSE;
static clock_t tstart;

static const char* r2s(int r)
{
    switch(r)
    {
        case EGL_SUCCESS: return "EGL_SUCCESS"; break;
        case EGL_NOT_INITIALIZED: return "EGL_NOT_INITIALIZED"; break;
        case EGL_BAD_ACCESS: return "EGL_BAD_ACCESS"; break;
        case EGL_BAD_ALLOC: return "EGL_BAD_ALLOC"; break;
        case EGL_BAD_ATTRIBUTE: return "EGL_BAD_ATTRIBUTE"; break;
        case EGL_BAD_CONFIG: return "EGL_BAD_CONFIG"; break;
        case EGL_BAD_CONTEXT: return "EGL_BAD_CONTEXT"; break;
        case EGL_BAD_CURRENT_SURFACE: return "EGL_BAD_CURRENT_SURFACE"; break;
        case EGL_BAD_DISPLAY: return "EGL_BAD_DISPLAY"; break;
        case EGL_BAD_MATCH: return "EGL_BAD_MATCH"; break;
        case EGL_BAD_NATIVE_PIXMAP: return "EGL_BAD_NATIVE_PIXMAP"; break;
        case EGL_BAD_NATIVE_WINDOW: return "EGL_BAD_NATIVE_WINDOW"; break;
        case EGL_BAD_PARAMETER: return "EGL_BAD_PARAMETER"; break;
        case EGL_BAD_SURFACE: return "EGL_BAD_SURFACE"; break;
        case EGL_CONTEXT_LOST: return "EGL_CONTEXT_LOST"; break;
        default: return "UNKNOWN";
    }
}

#define ASSERTEGL(exp){if((exp)==EGL_FALSE){LOG("EGLError %s",r2s(eglGetError()));goto ERROR;}}
#define ASSERT(exp){if(!(exp)){LOG("Error");goto ERROR;}}

int video_init(ANativeWindow *wnd, engine_video_t *out_video)
{
    if((video_display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY)
    {
        LOG("eglGetDisplay failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    if(eglInitialize(video_display, NULL, NULL) == EGL_FALSE)
    {
        LOG("eglInitialize failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    /* Here, the application chooses the configuration it desires. In this
     * sample, we have a very simplified selection process, where we pick
     * the first EGLConfig that matches our criteria */
    if(eglChooseConfig(video_display, video_attribs, &video_config, 1, &video_nconfigs) == EGL_FALSE)
    {
        LOG("eglInitialize failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    if(eglGetConfigAttrib(video_display, video_config, EGL_NATIVE_VISUAL_ID, &video_format) == EGL_FALSE)
    {
        LOG("eglGetConfigAttrib failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    ANativeWindow_setBuffersGeometry(wnd, 0, 0, video_format);
    video_wnd = wnd;

    if((video_surface = eglCreateWindowSurface(video_display, video_config, wnd, NULL)) == EGL_NO_SURFACE)
    {
        LOG("eglCreateWindowSurface failed: %s", r2s(eglGetError()));
        return FALSE;
    }
    if((video_context = eglCreateContext(video_display, video_config, NULL, NULL)) == EGL_NO_CONTEXT)
    {
        LOG("eglCreateContext failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    if(eglMakeCurrent(video_display, video_surface, video_surface, video_context) == EGL_FALSE)
    {
        LOG("eglMakeCurrent failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    if(eglQuerySurface(video_display, video_surface, EGL_WIDTH, &video_w) == EGL_FALSE)
    {
        LOG("eglMakeCurrent failed: %s", r2s(eglGetError()));
        return FALSE;
    }
    if(eglQuerySurface(video_display, video_surface, EGL_HEIGHT, &video_h) == EGL_FALSE)
    {
        LOG("eglMakeCurrent failed: %s", r2s(eglGetError()));
        return FALSE;
    }

    out_video->screen_width = video_w;
    out_video->screen_height = video_h;

    video_rendering = TRUE;

    tstart = engine_time_getms();

    return TRUE;
}

void video_destroy()
{
    if (video_display != EGL_NO_DISPLAY)
    {
        eglMakeCurrent(video_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (video_context != EGL_NO_CONTEXT)
        {
            eglDestroyContext(video_display, video_context);
        }
        if (video_surface != EGL_NO_SURFACE)
        {
            eglDestroySurface(video_display, video_surface);
        }
        eglTerminate(video_display);
    }

    if(video_wnd != NULL)
        ANativeWindow_setBuffersGeometry(video_wnd, 0, 0, 0);

    video_display = EGL_NO_DISPLAY;
    video_context = EGL_NO_CONTEXT;
    video_surface = EGL_NO_SURFACE;
    video_rendering = FALSE;

    LOG("video destroyed");
}

void video_update()
{
    static int time = 0;
    clock_t tstop = engine_time_getms();
    int ms = (tstop - tstart);
    tstart = tstop;

    if(video_rendering == FALSE)
    {
        LOG("non rendering");
        return;
    }

    time += ms;
    if(time >= 33)
    {
        //LOG("render");
        app_draw(time);
        eglSwapBuffers(video_display, video_surface);
        time = 0;
    }
    else
    {
        //LOG("idle for %d ms", (33 - time));
        usleep((33 - time) * 1000);
    }

}
