#include "engine.h"

float volume_clamp(float volume);

static AVAudioPlayer *mplayer = NULL;
static ALCcontext *al_context = NULL;
static ALCdevice *al_device = NULL;

#define BUFFER_MAX 32
#define PLAYER_MAX 32

typedef struct sound_buffer_t
{
    ALuint buffer;
    char used;
} sound_buffer_t;
typedef struct sound_player_t
{
    ALuint source;
    char used;
} sound_player_t;

sound_buffer_t sbuffer[BUFFER_MAX];
sound_player_t splayer[PLAYER_MAX];
float g_volume;
float mvolume;

int engine_audio_init()
{
    al_device = alcOpenDevice(NULL);
    if(al_device == NULL) return FALSE;
    al_context = alcCreateContext(al_device, NULL);
    if(al_context == NULL) return FALSE;
    if(alcMakeContextCurrent(al_context) == AL_FALSE) return FALSE;
    memset(sbuffer, 0, sizeof(sbuffer));
    memset(splayer, 0, sizeof(splayer));
    g_volume = 1.0f;
    return TRUE;
}

int engine_audio_destroy()
{
    int i;
    for(i = 0; i < BUFFER_MAX; i++) alDeleteBuffers(1, &sbuffer[i].buffer);
    for(i = 0; i < PLAYER_MAX; i++) alDeleteSources(1, &splayer[i].source);
    alcMakeContextCurrent(NULL);
    alcDestroyContext(al_context);
    alcCloseDevice(al_device);
    return TRUE;
}

float volume_clamp(float volume)
{
    if(volume < 0.0f) return 0.0f;
    if(volume > 1.0f) return 1.0f;
    return volume;
}

int engine_audio_volume_set(float volume)
{
    g_volume = volume_clamp(volume);
    alListenerf(AL_GAIN, g_volume);
    mplayer.volume = mvolume * g_volume;
    return TRUE;
}

int engine_audio_volume_offset(float volume)
{
    return engine_audio_volume_set(g_volume + volume);
}

int engine_audio_music_load(const char *filename)
{
    NSError *err = NULL;
    NSString *file =  [NSString stringWithCString:filename encoding:NSUTF8StringEncoding];
    NSString *path = [[NSBundle mainBundle] pathForResource:file ofType:nil];
    NSURL *url = [NSURL fileURLWithPath:path];
    mplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&err];
    if(err)
    {
        LOG("%@", err);
        return FALSE;
    }
    return TRUE;
}

int engine_audio_music_play(int id, int loop, float volume)
{
    [mplayer play];
    mplayer.volume = volume * g_volume;
    mvolume = volume;
    mplayer.numberOfLoops = loop ? -1 : 0;
    return TRUE;
}

int engine_audio_music_stop(int id)
{
    [mplayer stop];
    return TRUE;
}

static int buffer_get()
{
    int i;
    for(i = 0; i < BUFFER_MAX; i++)
    {
        if(sbuffer[i].used == 0) return i;
    }
    return -1;
}

static int player_get()
{
    int i, empty = -1;
    ALint status;
    for(i = 0; i < BUFFER_MAX; i++)
    {
        if(splayer[i].used)
        {
            alGetSourcei(splayer[i].source, AL_SOURCE_STATE, &status);
            if(status != AL_PLAYING)
            {
                LOG("reuse player");
                return i;
            }
        }
        else if(empty == -1)
        {
            empty = i;
        }
    }
    if(empty > -1)
    {
        LOG("create player");
        alGenSources(1, &splayer[empty].source);
        splayer[empty].used = 1;
        return empty;
    }
    return -1;    
}

int engine_audio_sound_load(const char *filename)
{
    int bi = buffer_get();
    if(bi == -1) return -1;
    
    alGenBuffers(1, &sbuffer[bi].buffer);
    
    FILE *fp = engine_asset_open(filename);
    
    fseek(fp, 0, SEEK_END);
    long len = ftell(fp);
    rewind(fp);
    
    ALubyte *mem = (ALubyte*)malloc(len);
    fread(mem, len, 1, fp);
    fclose(fp);
    alBufferData(sbuffer[bi].buffer, AL_FORMAT_MONO16, mem, (ALsizei)len, 44100);
    free(mem);
    
    sbuffer[bi].used = 1;
    
    return bi;
}

int engine_audio_sound_play(int id, float volume)
{
    int pid = player_get();
    if(pid == -1) return FALSE;
    alSourcef(splayer[pid].source, AL_GAIN, volume);
    alSourcei(splayer[pid].source, AL_BUFFER, sbuffer[id].buffer);
    alSourcePlay(splayer[pid].source);
    return TRUE;
}
