#ifndef _GFX_GUI_
#define _GFX_GUI_

#include "engine_primitives.h"

#define GFX_GUI_MAX_HANDLE 	100

#define GFX_GUI_UNHANDLED	0
#define GFX_GUI_HANDLED		1

typedef int T_GUI;
extern T_GUI T_GUI_TOUCH_DOWN;
extern T_GUI T_GUI_TOUCH_UP;
extern T_GUI T_GUI_TOUCH_MOVE;
extern T_GUI T_GUI_TOUCH_CLICK;
extern T_GUI T_GUI_KNOB_VALUE;
extern T_GUI T_GUI_KNOB_END;
extern T_GUI T_GUI_KNOB_TICK;

class gfxGuiObject : public gfxObject
{
public:
	float width;
	float height;
	int pointer_id;
	gfxGuiObject *parent;

	gfxGuiObject();
	virtual ~gfxGuiObject();

	virtual void draw();
	virtual int on_touch(T_GUI type, float x, float y, int id);
	virtual void handle_touch();
	virtual void unhandle_touch();

	int hit_test(float x, float y);
	gfxVector2D absolute_pos();
};
////////////////////////////////////////////////////////////////////
class gfxGui
{
public:
	static gfxBitmapFont defaultFont;
	static gfxGuiObject *handled[];
	static int handled_count;

	static void init_handler();
	static void handle_add(gfxGuiObject *obj);
	static void handle_remove(gfxGuiObject *obj);
	static int handle_touch(T_GUI type, float x, float y, int id);
};
////////////////////////////////////////////////////////////////////
class gfxGuiDelegate
{
public:
	virtual ~gfxGuiDelegate();
	virtual void on_gui_delegate(class gfxGuiObject *sender, T_GUI event, void *data);
};
////////////////////////////////////////////////////////////////////
class gfxGuiButton : public gfxGuiObject
{
public:
	static gfx9Slice defaultTheme;

	gfxColor buttonColor;
	gfxColor labelColor;
	gfxGuiDelegate * delegate;
	const char *label;
	float text_width;
	int down, inside, visible;

	gfxGuiButton();
	virtual ~gfxGuiButton();

	virtual void draw();
	virtual int on_touch(T_GUI type, float x, float y, int id);
	void set_text(const char *t);
};
////////////////////////////////////////////////////////////////////
class gfxGuiButtonPush : public gfxGuiButton
{
public:
	int pressed;
	
	gfxGuiButtonPush();
	
	int on_touch(T_GUI type, float x, float y, int id);
};
////////////////////////////////////////////////////////////////////
class gfxGuiButtonImage : public gfxGuiObject
{
public:
	gfxSpriteMultipart image;
	gfxColor buttonColor;
	gfxGuiDelegate * delegate;
	int down, inside;
	
	gfxGuiButtonImage();
	virtual ~gfxGuiButtonImage();
	
	void draw();
	void set_image(gfxSpriteMultipart *img);
	int on_touch(T_GUI type, float x, float y, int id);
};
////////////////////////////////////////////////////////////////////
class gfxGuiGraph : public gfxGuiObject
{
public:
	gfxVector2D *_data;
	int count;
	gfxColor color;
	
	gfxGuiGraph();
	
	void init(int n, gfxVector2D *data, float w, float h);
	void push(float value);
	void draw();
};
////////////////////////////////////////////////////////////////////
class gfxGuiKnob : public gfxGuiObject
{
public:
	static int snd_tick;
	typedef gfxGuiObject super;
	gfxSpriteMultipart bg, tick;
	gfxGuiDelegate * delegate;
	int down, inside, ntick;
	float value;
	
	gfxGuiKnob();
	
	void draw();
	void set_value(float v);
	void play_tick();
	int on_touch(T_GUI type, float x, float y, int id);
};
#endif
