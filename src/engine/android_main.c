#include "android_core.h"

#define MSG_CREATE      1
#define MSG_START       2
#define MSG_RESUME      3
#define MSG_PAUSE       4
#define MSG_STOP        5
#define MSG_DESTROY     6

#define MSG_SAVE        7
#define MSG_LOWMEM      8
#define MSG_WNDFOCUS    9
#define MSG_WNDCREATE   10
#define MSG_WNDDESTROY  11
#define MSG_IQCREATE    12
#define MSG_IQDESTROY   13

#define ID_INPUT   1
#define ID_PIPE    2
#define ID_SENSOR1 3
#define ID_SENSOR2 4

#define PowerManager_PARTIAL_WAKE_LOCK          1L
#define PowerManager_FULL_WAKE_LOCK             26L
#define PowerManager_SCREEN_BRIGHT_WAKE_LOCK    10L
#define PowerManager_SCREEN_DIM_WAKE_LOCK       6L
#define PowerManager_ACQUIRE_CAUSES_WAKEUP      268435456L
#define PowerManager_ON_AFTER_RELEASE           536870912L

// local prototype
static void* thread_main(void *data);

// local variables
static pthread_t thread;
static int ok_input = 0;
static int ok_wnd = 0;
// thread control
static int active = 0;
static int focus = 0;
static pthread_mutex_t paused_mutex;
static pthread_cond_t paused_cond;

// global objects
ALooper             *android_looper     = NULL;
AInputQueue         *android_iq         = NULL;
ANativeWindow       *android_wnd        = NULL;
AAssetManager       *android_am         = NULL;
ANativeActivity     *android_activity   = NULL;

// terminate activity
void engine_terminate()
{
    ANativeActivity_finish(android_activity);
}

static void onPause(ANativeActivity *activity)
{
    LOG("onPause");
    ipc_message_post(MSG_PAUSE);
}

static void onDestroy(ANativeActivity *activity)
{
    LOG("onDestroy");
    ipc_message_post(MSG_DESTROY);
}

static void onInputQueueCreated(ANativeActivity* activity, AInputQueue* queue)
{
    LOG("onInputQueueCreated");
    android_iq = queue;
    ipc_message_post(MSG_IQCREATE);

    ipc_mutex_lock();
    ok_input = 1;
    ipc_cond_broadcast();
    ipc_mutex_unlock();
}

static void onInputQueueDestroied(ANativeActivity *activity, AInputQueue *queue)
{
    LOG("onInputQueueDestroied");
    ipc_message_post(MSG_IQDESTROY);
}

static void onNativeWindowCreated(ANativeActivity *activity, ANativeWindow *window)
{
    LOG("onNativeWindowCreated");
    android_wnd = window;
    ipc_message_post(MSG_WNDCREATE);

    ipc_mutex_lock();
    ok_wnd = 1;
    ipc_cond_broadcast();
    ipc_mutex_unlock();
}

static void onNativeWindowDestroyed(ANativeActivity *activity, ANativeWindow *window)
{
    LOG("onNativeWindowDestroyed");
    android_wnd = NULL;
    ipc_message_post(MSG_WNDDESTROY);
}

static void* onSaveInstanceState(ANativeActivity* activity, size_t *outSize)
{
    LOG("onSaveInstanceState");
    ipc_message_post(MSG_SAVE);
    return 0;
}
static void onStart(ANativeActivity* activity)
{
    LOG("onStart");
    ipc_message_post(MSG_START);
}
static void onStop(ANativeActivity* activity)
{
    LOG("onStop");
    ipc_message_post(MSG_STOP);
}
static void onResume(ANativeActivity* activity)
{
    LOG("onResume");
    ipc_message_post(MSG_RESUME);
}
static void onLowMemory(ANativeActivity* activity)
{
    LOG("onLowMemory");
    ipc_message_post(MSG_LOWMEM);
}
static void onWindowFocusChanged(ANativeActivity* activity, int hasFocus)
{
    LOG("onWindowFocusChanged");
    ipc_message_post(MSG_WNDFOCUS);
    focus = hasFocus;
}

void ANativeActivity_onCreate(ANativeActivity *activity, void *data, size_t sz)
{
    ERR("-------------------------------------------------");
    activity->callbacks->onLowMemory = onLowMemory;
    activity->callbacks->onResume = onResume;
    activity->callbacks->onSaveInstanceState = onSaveInstanceState;
    activity->callbacks->onStart = onStart;
    activity->callbacks->onStop = onStop;
    activity->callbacks->onWindowFocusChanged = onWindowFocusChanged;
    activity->callbacks->onPause = onPause;
    activity->callbacks->onDestroy = onDestroy;
    activity->callbacks->onInputQueueCreated = onInputQueueCreated;
    activity->callbacks->onInputQueueDestroyed = onInputQueueDestroied;
    activity->callbacks->onNativeWindowCreated = onNativeWindowCreated;
    activity->callbacks->onNativeWindowDestroyed = onNativeWindowDestroyed;
    android_am = activity->assetManager;

    JNIEnv *env = activity->env;
    jclass jActivity = (*env)->GetObjectClass(env, activity->clazz);
    jmethodID jActivity_setVolumeControlStream =
        (*env)->GetMethodID(env, jActivity, "setVolumeControlStream", "(I)V");
    jmethodID jActivity_getSystemService =
        (*env)->GetMethodID(env, jActivity, "getSystemService",
        "(Ljava/lang/String;)Ljava/lang/Object;");

    (*env)->CallVoidMethod(env, activity->clazz, jActivity_setVolumeControlStream, 3);

    jstring serviceName = (*env)->NewStringUTF(env, "power");
    jobject powerManager = (*env)->CallObjectMethod(env, activity->clazz,
        jActivity_getSystemService, serviceName);
    jclass jPowerManager = (*env)->GetObjectClass(env, powerManager);

    jstring tag = (*env)->NewStringUTF(env, "native");
    jmethodID methodID = (*env)->GetMethodID(env, jPowerManager, "newWakeLock",
        "(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;");
    LOG("newWakeLock id %p", methodID);
    jobject wakeLock = (*env)->CallObjectMethod(env, powerManager, methodID,
        PowerManager_SCREEN_BRIGHT_WAKE_LOCK | PowerManager_ACQUIRE_CAUSES_WAKEUP, tag);
    jclass jWakeLock = (*env)->GetObjectClass(env, wakeLock);

    jmethodID jWakeLock_acquire = (*env)->GetMethodID(env, jWakeLock, "acquire", "()V");
    jmethodID jWakeLock_release = (*env)->GetMethodID(env, jWakeLock, "release", "()V");

    (*env)->CallVoidMethod(env, wakeLock, jWakeLock_acquire);

    (*env)->DeleteLocalRef(env, serviceName);
    (*env)->DeleteLocalRef(env, tag);

    ipc_init();

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_create(&thread, &attr, thread_main, 0);
}

static void* thread_main(void *data)
{
    active = 1;

    // init thread control
    pthread_mutex_init(&paused_mutex, NULL);
    pthread_cond_init(&paused_cond, NULL);

    // init the looper for this thread
    android_looper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);

    // add the ipc pipe to the looper
    ALooper_addFd(android_looper, ipc_pipe_r(), ID_PIPE, ALOOPER_EVENT_INPUT, NULL, NULL);

    LOG("wait android");

    // wait until window and input queue is created
    ipc_mutex_lock();
    while (android_wnd == NULL || android_iq == NULL)
        ipc_cond_wait();
    ipc_mutex_unlock();

    LOG("ready to init");

    // call custom application initializzation
    app_main();

    // reset touch buffers
    touch_reset();

    while (1)
    {
        int ident;
        int fdesc;
        int events;
        engine_video_t video;
        char msg;

        while ((ident = ALooper_pollAll(active ? 0 : -1, NULL, &events, NULL)) >= 0)
        {
            switch (ident)
            {
            case ID_INPUT:
                handle_input(android_iq);
                break;
            case ID_PIPE:
                msg = ipc_message_get();
                switch (msg)
                {
                case MSG_IQCREATE:
                    AInputQueue_attachLooper(android_iq, android_looper, ID_INPUT, NULL, NULL);
                    break;
                case MSG_IQDESTROY:
                    AInputQueue_detachLooper(android_iq);
                    android_iq = NULL;
                    break;
                case MSG_WNDCREATE:
                    video_init(android_wnd, &video);
                    engine_audio_init();
                    LOG("initting app");
                    app_init(&video);
                    active = 1;
                    break;
                case MSG_PAUSE:
                    active = 0;
                    app_pause();
                    engine_audio_destroy();
                    video_destroy();
                    break;
                case MSG_WNDDESTROY:
                    break;
                case MSG_DESTROY:
                    app_destroy();
                    break;
                case MSG_RESUME:
                    LOG("resume message");
                    break;
                }
                break;
            }
        }
        if(active && focus) video_update();
        else LOG("empty loop");
    }
    return 0;
}
