#include "android_core.h"

extern AAssetManager *android_am;

FILE* engine_asset_open(const char *filename)
{
    long off, len;
    FILE *fp = NULL;
    AAsset *as = AAssetManager_open(android_am, filename, AASSET_MODE_STREAMING);
    int fd = AAsset_openFileDescriptor(as, &off, &len);
    if(fd >= 0)
    {
        fp = fdopen(fd, "rb");
        fseek(fp, off, SEEK_CUR);
    }
    AAsset_close(as);
    return fp;
}

int android_asset_fd(const char *filename, long *offset, long *length)
{
    long off, len;
    AAsset *as = AAssetManager_open(android_am, filename, AASSET_MODE_STREAMING);
    int fd = AAsset_openFileDescriptor(as, &off, &len);
    AAsset_close(as);
    if(offset) *offset = off;
    if(length) *length = len;
    return fd;
}
