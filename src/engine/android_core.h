#include "engine.h"

typedef struct android_music_t
{
    SLObjectItf player_obj;
    SLPlayItf player;
    SLSeekItf seek;
    SLVolumeItf volume;
    float fvol;
} android_music_t;

typedef struct android_sound_buffer_t
{
    SLuint8 *data;
    SLuint32 size;
} android_sound_buffer_t;

typedef struct android_sound_player_t
{
    SLPlayItf player;
    SLObjectItf player_obj;
    SLBufferQueueItf queue;
    SLVolumeItf volume;
    float fvol;
} android_sound_player_t;

extern void touch_reset();
extern int  touch_remove(int id);
extern int  touch_update(int id, int x, int y);
extern int  touch_index(int id);
extern int  touch_add(int id, int x, int y);

extern void ipc_init();
extern int  ipc_pipe_r();
extern void ipc_mutex_lock();
extern void ipc_mutex_unlock();
extern void ipc_cond_wait();
extern void ipc_cond_broadcast();
extern void ipc_message_post(char cmd);
extern char ipc_message_get();

extern void handle_input(AInputQueue *iq);

extern int  video_init(ANativeWindow *wnd, engine_video_t *out_video);
extern void video_update();

extern int android_asset_fd(const char *filename, long *offset, long *length);

extern ALooper       *android_looper;
extern AInputQueue   *android_iq;
extern ANativeWindow *android_wnd;
extern AAssetManager *android_am;
