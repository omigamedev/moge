#include "engine_3dmath.h"

float clamp(float f, float min, float max)
{
	if(f < min) return min;
	if(f > max) return max;
	return f;
}
float clampc(float c)
{
	if(c < 0.0f) return 0.0f;
	if(c > 1.0f) return 1.0f;
	return c;
}
float interpolate_linear_in(float t, float a, float b)
{
	return (b - a) * t + a;
}
float interpolate_linear_out(float t, float a, float b)
{
	t = 1 - t;
	return (b - a) * (1 - t) + a;
}
float interpolate_quadratic_in(float t, float a, float b)
{
	return (b - a) * (t * t) + a;
}
float interpolate_quadratic_out(float t, float a, float b)
{
	t = 1 - t;
	return (b - a) * (1 - (t * t)) + a;
}
float randf()
{
	static float div = 1.0f / (float)RAND_MAX;
	return (float)(rand() % RAND_MAX) * div;
}
float randfneg()
{
    static float div = 2.0f / (float) RAND_MAX;
    return (float) (rand() % RAND_MAX) * div - 1.0f;
}

gfxVector2D::gfxVector2D() : x(0.0f), y(0.0f){}
gfxVector2D::gfxVector2D(float xy) : x(xy), y(xy){}
gfxVector2D::gfxVector2D(float x, float y) : x(x), y(y){}

gfxScreenCoord::gfxScreenCoord() : x(0), y(0){}
gfxScreenCoord::gfxScreenCoord(int x, int y) : x(x), y(y){}

gfxTexCoord::gfxTexCoord() : u(0.0f), v(0.0f) { }
gfxTexCoord::gfxTexCoord(float uv) : u(uv), v(uv){}
gfxTexCoord::gfxTexCoord(float u, float v) : u(u), v(v){}

gfxColor::gfxColor() : r(0.5f), g(0.5f), b(0.5f), a(1.0f){}
gfxColor::gfxColor(float r, float g, float b, float a) : r(r), g(g), b(b), a(a){}
gfxColor::gfxColor(int r, int g, int b, int a)
{
	static float inv = 1.0f / 255.0f;
	this->r = r * inv;
	this->g = g * inv;
	this->b = b * inv;
	this->a = a * inv;
}
