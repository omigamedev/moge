#include "engine_gui.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

T_GUI T_GUI_TOUCH_DOWN		= 0;
T_GUI T_GUI_TOUCH_UP		= 1;
T_GUI T_GUI_TOUCH_MOVE		= 2;
T_GUI T_GUI_TOUCH_CLICK		= 3;
T_GUI T_GUI_KNOB_VALUE		= 4;
T_GUI T_GUI_KNOB_END		= 5;
T_GUI T_GUI_KNOB_TICK		= 6;

gfxBitmapFont gfxGui::defaultFont;
gfxGuiObject* gfxGui::handled[GFX_GUI_MAX_HANDLE];
int gfxGui::handled_count = 0;

void gfxGui::init_handler()
{
	memset(handled, 0, sizeof(handled));
}
void gfxGui::handle_add(gfxGuiObject *obj)
{
	if(handled_count == GFX_GUI_MAX_HANDLE) return;
	for(int i = 0; i < GFX_GUI_MAX_HANDLE; i++)
	{
		if(handled[i] == 0)
		{
			handled[i] = obj;
			handled_count++;
			return;
		}
	}
}
void gfxGui::handle_remove(gfxGuiObject *obj)
{
	for(int i = 0; i < GFX_GUI_MAX_HANDLE; i++)
	{
		if(handled[i] == obj)
		{
			handled[i] = 0;
			handled_count--;
			return;
		}
	}
}
int gfxGui::handle_touch(T_GUI type, float x, float y, int id)
{
	int ret = 0;
	for(int i = GFX_GUI_MAX_HANDLE - 1; i >= 0; i--)
	{
		if(handled[i] != 0)
		{
			ret += handled[i]->on_touch(type, x, y, id);
			if(ret) break;
		}
	}
	return ret;
}
////////////////////////////////////////////////////////////////////
gfxGuiObject::~gfxGuiObject(){}
gfxGuiObject::gfxGuiObject()
{
	parent = 0;
	pointer_id = -1;
}
void gfxGuiObject::draw(){}
int gfxGuiObject::on_touch(T_GUI type, float x, float y, int id)
{
	return GFX_GUI_UNHANDLED;
}
void gfxGuiObject::handle_touch()
{
	gfxGui::handle_add(this);
}
void gfxGuiObject::unhandle_touch()
{
	gfxGui::handle_remove(this);
}
int gfxGuiObject::hit_test(float x, float y)
{
	gfxVector2D pos = absolute_pos();
	return x > pos.x - width &&	x < pos.x + width &&
		y > pos.y - height && y < pos.y + height;
}
gfxVector2D gfxGuiObject::absolute_pos()
{
	gfxVector2D pos = this->pos;
	gfxGuiObject *obj = this;
	while((obj = obj->parent)) 
	{
		pos.x += obj->pos.x;
		pos.y += obj->pos.y;
	}
	return pos;
}
////////////////////////////////////////////////////////////////////
gfxGuiDelegate::~gfxGuiDelegate(){}
void gfxGuiDelegate::on_gui_delegate(class gfxGuiObject *sender, T_GUI event, void *data = 0){}
////////////////////////////////////////////////////////////////////
gfx9Slice gfxGuiButton::defaultTheme;
gfxGuiButton::gfxGuiButton()
{
	label = 0;
	width = 1.0f;
	height = 1.0f;
	text_width = 0.0f;
	down = inside = 0;
	delegate = 0;
	buttonColor.a = 0.7f;
	visible = 1;
}
gfxGuiButton::~gfxGuiButton()
{
	gfxGui::handle_remove(this);
}
void gfxGuiButton::draw()
{
	if(visible == 0) return;
	if(down && inside)
	{
		glColor4f(clampc(buttonColor.r + 0.3f), clampc(buttonColor.r + 0.3f),
			clampc(buttonColor.r + 0.3f), buttonColor.a);
	}
	else
	{
		glColor4f(buttonColor.r, buttonColor.g, buttonColor.b, buttonColor.a);
	}
	defaultTheme.sx = width;
	defaultTheme.sy = height;
	defaultTheme.pos = pos;
	defaultTheme.draw();

	if(label)
	{
		glColor4f(labelColor.r, labelColor.g, labelColor.b, labelColor.a);
		gfxGui::defaultFont.draw_text_center(label, pos.x, pos.y);
	}
}
void gfxGuiButton::set_text(const char *t)
{
	width = text_width * 1.1f;
	height = gfxGui::defaultFont.height() * 2.5f;
	label = t;
}
int gfxGuiButton::on_touch(T_GUI type, float x, float y, int id)
{
	//LOGI("%p pointer %d\n", this, id);
	int ret = GFX_GUI_UNHANDLED;
	if(type == T_GUI_TOUCH_DOWN)
	{
		down = inside = hit_test(x, y);
		ret = inside;
		pointer_id = id;
	}
	else if(type == T_GUI_TOUCH_MOVE && id == pointer_id)
	{
		inside = hit_test(x, y);
		ret = down;
	}
	else if(type == T_GUI_TOUCH_UP && id == pointer_id)
	{
		if(delegate && down && inside)
		{
			delegate->on_gui_delegate(this, T_GUI_TOUCH_CLICK, 0);
		}
		ret = down;
		down = inside = 0;
		pointer_id = -1;
	}
	if(ret && delegate && id == pointer_id) delegate->on_gui_delegate(this, type, 0);
	return ret;
}
////////////////////////////////////////////////////////////////////
gfxGuiButtonPush::gfxGuiButtonPush()
{
	pressed = 0;
}
int gfxGuiButtonPush::on_touch(T_GUI type, float x, float y, int id)
{
	int ret = GFX_GUI_UNHANDLED;
	if(pressed) return ret;
	if(type == T_GUI_TOUCH_DOWN)
	{
		down = inside = hit_test(x, y);
		ret = inside;
	}
	else if(type == T_GUI_TOUCH_MOVE)
	{
		inside = hit_test(x, y);
		ret = down;
	}
	else if(type == T_GUI_TOUCH_UP)
	{
		ret = down;
		if(down && inside)
		{
			if(delegate) delegate->on_gui_delegate(this, T_GUI_TOUCH_CLICK, 0);
			pressed = 1;
		}
		else
		{
			down = inside = 0;
		}
	}
	if(ret && delegate) delegate->on_gui_delegate(this, type, 0);
	return ret;
}
////////////////////////////////////////////////////////////////////
gfxGuiButtonImage::gfxGuiButtonImage()
{
	width = 1.0f;
	height = 1.0f;
	down = inside = 0;
	delegate = 0;
}
gfxGuiButtonImage::~gfxGuiButtonImage()
{
	gfxGui::handle_remove(this);
}
void gfxGuiButtonImage::draw()
{
	if(down && inside)
	{
		glColor4f(clampc(buttonColor.r + 0.3f), clampc(buttonColor.r + 0.3f),
				  clampc(buttonColor.r + 0.3f), buttonColor.a);
	}
	else
	{
		glColor4f(buttonColor.r, buttonColor.g, buttonColor.b, buttonColor.a);
	}
	image.pos = pos;
	image.draw();
}
int gfxGuiButtonImage::on_touch(T_GUI type, float x, float y, int id)
{
	int ret = GFX_GUI_UNHANDLED;
	if(type == T_GUI_TOUCH_DOWN)
	{
		down = inside = hit_test(x, y);
		ret = inside;
	}
	else if(type == T_GUI_TOUCH_MOVE)
	{
		inside = hit_test(x, y);
		ret = down;
	}
	else if(type == T_GUI_TOUCH_UP)
	{
		if(delegate && down && inside)
		{
			delegate->on_gui_delegate(this, T_GUI_TOUCH_CLICK, 0);
		}
		ret = down;
		down = inside = 0;
	}
	if(ret && delegate) delegate->on_gui_delegate(this, type, 0);
	return ret;
}
////////////////////////////////////////////////////////////////////
gfxGuiGraph::gfxGuiGraph()
{
	color = gfxColor(0.5f, 0.5f, 0.5f, 1.0f);
}
void gfxGuiGraph::init(int n, gfxVector2D *data, float w, float h)
{
	count = n;
	width = w;
	height = h;
	_data = data;
	float space = w / (float)(n-1);
	for(int i = 0; i < n; i++)
	{
		_data[i].x = space * i - w * 0.5f;
	}
}
void gfxGuiGraph::push(float value)
{
	for(int i = 0; i < count - 1; i++)
	{
		_data[i].y = _data[i + 1].y;
	}
	_data[count - 1].y = value;
}
void gfxGuiGraph::draw()
{
	glPushMatrix();
	glTranslatef(pos.x, pos.y, 0.0f);
	glVertexPointer(2, GL_FLOAT, 0, _data);
	glBindTexture(GL_TEXTURE_2D, 0);
	glColor4f(color.r, color.g, color.b, color.a);
	glDrawArrays(GL_LINE_STRIP, 0, count);
//	glPointSize(3.0f);
//	glDrawArrays(GL_POINTS, 0, count);
	glPopMatrix();
}
////////////////////////////////////////////////////////////////////
int gfxGuiKnob::snd_tick = -1;
gfxGuiKnob::gfxGuiKnob()
{
	down = 0;
	inside = 0;
	delegate = 0;
	value = 0.0f;
	set_value(0.0f);
	ntick = -1;
}
void gfxGuiKnob::draw()
{
	gfxBitmapFont font = gfxGui::defaultFont;
	font.scale = 0.04;
	bg.scale = tick.scale = width;
	
	if(ntick == 0)
	{
		ntick = 5; // speed of the tick
		play_tick();
	}
	else if(ntick > 0)
	{
		ntick--;
	}

	glPushMatrix();
	glTranslatef(pos.x, pos.y, 0.0f);
	bg.draw();
	tick.draw();
	char c[2];
	for(int i = 0; i < 10; i++)
	{
		sprintf(c, "%2d", 9 - i);
		glPushMatrix();
		glRotatef(i * 22.5f, 0.0f, 0.0f, 1.0f);
		glTranslatef(width * 1.0f, font.text_width(c) * 0.5f, 0.0f);
		glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
		font.draw_text(c, 0.0f, 0.0f);
		glPopMatrix();
	}
	glPopMatrix();
}
int gfxGuiKnob::on_touch(T_GUI type, float x, float y, int id)
{
	static float start_angle = 0.0f;
	static float tick_value;
	static gfxVector2D start;
	int ret = GFX_GUI_UNHANDLED;

	if(delegate) delegate->on_gui_delegate(this, type, 0);
	if(type == T_GUI_TOUCH_DOWN)
	{
		down = inside = hit_test(x, y);
		ret = inside;
		start_angle = tick.rot;
		tick_value = value;
		start.x = x;
		start.y = y;
		ntick = inside ? 0 : -1;
	}
	else if(type == T_GUI_TOUCH_MOVE)
	{
		inside = hit_test(x, y);
		ret = down;
		if(down)
		{
			//TODO: use angle difference
			tick.rot = clamp(start_angle + (start.y - y) * 100, -90.0f, -90.0f + 22.5f * 9);
			value = (22.5f * 5 - tick.rot) / (22.5f * 9);
			if(delegate) delegate->on_gui_delegate(this, T_GUI_KNOB_VALUE, 0);
			if(fabsf(tick_value - value) > 0.05f)
			{
				//play_tick();
				tick_value = value;
				if(delegate) delegate->on_gui_delegate(this, T_GUI_KNOB_TICK, 0);
			}
		}
	}
	else if(type == T_GUI_TOUCH_UP)
	{
		ret = down;
		if(delegate && down) delegate->on_gui_delegate(this, T_GUI_KNOB_END, 0);
		down = inside = 0;
		ntick = -1;
	}
	return ret;
}
void gfxGuiKnob::set_value(float v)
{
	tick.rot = (1.0f - v) * (22.5f * 9) - 90.0f;
	value = v;
}
void gfxGuiKnob::play_tick()
{
	if(snd_tick == -1)
	{
		//snd_tick = gfxApplication::singleton->loadSound("Media/tick.wav");
	}
	else 
	{
		//gfxApplication::singleton->playSound(snd_tick, 1.0f);
	}
}
////////////////////////////////////////////////////////////////////
