#ifndef _GFX_TEXTURE_
#define _GFX_TEXTURE_

#include "engine.h"

class gfxTexture
{
public:
	GLuint id;
	int width;
	int height;

	gfxTexture();

	void load(const char *filename);
	void unload();
};

#endif
