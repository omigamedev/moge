#include "android_core.h"

static pthread_mutex_t mutex;
static pthread_cond_t cond;
static int ipc_pipe[2];

void ipc_init()
{
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond, NULL);
    pipe(ipc_pipe);
}

void ipc_destroy()
{
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond, NULL);
    close(ipc_pipe[0]);
    close(ipc_pipe[1]);
}

int ipc_pipe_r()
{
    return ipc_pipe[0];
}

void ipc_mutex_lock()
{
    pthread_mutex_lock(&mutex);
}

void ipc_mutex_unlock()
{
    pthread_mutex_unlock(&mutex);
}

void ipc_cond_wait()
{
    pthread_cond_wait(&cond, &mutex);
}

void ipc_cond_broadcast()
{
    pthread_cond_broadcast(&cond);
}

void ipc_message_post(char cmd)
{
    write(ipc_pipe[1], &cmd, sizeof(cmd));
}

char ipc_message_get()
{
    char cmd;
    read(ipc_pipe[0], &cmd, sizeof(cmd));
    return cmd;
}
