LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := libengine

LOCAL_SRC_FILES := \
android_audio.c \
android_events.c \
android_file.c \
android_ipc.c \
android_main.c \
android_touch.c \
android_video.c \
texture.c \
time.c \
engine_3dmath.cpp \
engine_gui.cpp \
engine_primitives.cpp \
engine_scene.cpp \
engine_texture.cpp

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/..
LOCAL_C_INCLUDES := $(LOCAL_PATH)/..
LOCAL_CFLAGS := -DENGINE_ANDROID
LOCAL_STATIC_LIBRARIES := png

include $(BUILD_STATIC_LIBRARY)

