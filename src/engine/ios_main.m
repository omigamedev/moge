#include "engine.h"

@interface GLView : UIView <UIAccelerometerDelegate>
{
    EAGLContext *_context;
    GLuint _framebuffer, _renderbuffer;
    CADisplayLink *displayLink;
    clock_t tstart;
}
- (void)drawView: (CADisplayLink *) displayLink;
@end

@implementation GLView
- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        CAEAGLLayer *eaglLayer = (CAEAGLLayer*) super.layer;
        eaglLayer.opaque = YES;
        
        _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
        
        if (!_context || ![EAGLContext setCurrentContext:_context]) 
        {
            [self release];
            return nil;
        }
        
        glGenRenderbuffersOES(1, &_renderbuffer);
        glBindRenderbufferOES(GL_RENDERBUFFER_OES, _renderbuffer);
        
        [_context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:eaglLayer];
        glGenFramebuffersOES(1, &_framebuffer);
        glBindFramebufferOES(GL_FRAMEBUFFER_OES, _framebuffer);
        glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, _renderbuffer);
        
        engine_video_t video;
        video.screen_width = frame.size.width;
        video.screen_height = frame.size.height;
        app_init(&video);
        
        displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(drawView:)];
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        displayLink.frameInterval = 2;
        
        self.multipleTouchEnabled = YES;
        
        [[UIAccelerometer sharedAccelerometer] setUpdateInterval:(1.0 / 30.0)];
        [[UIAccelerometer sharedAccelerometer] setDelegate:self];

        tstart = engine_time_getms();
    }
    return self;
}
- (void) drawView: (CADisplayLink*) displayLink
{
    clock_t tstop = engine_time_getms();
    app_draw((tstop - tstart));
    tstart = tstop;
    [_context presentRenderbuffer:GL_RENDERBUFFER_OES];
}
- (void)dealloc 
{
    [_context release];
    [super dealloc];
}
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}
- (void)viewDidUnload
{    
    [displayLink invalidate];
    if ([EAGLContext currentContext] == _context) 
    {
        [EAGLContext setCurrentContext:nil];
    }
    [_context release];
    _context = nil;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
    for (UITouch *t in touches)
    {
        CGPoint startPoint = [t locationInView:self];
        CGFloat x = startPoint.x;
        CGFloat y = startPoint.y;
        app_touch_down((int)x, (int)y, (int)t);
        i++;
    }
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
    for (UITouch *t in touches)
    {
        CGPoint startPoint = [t locationInView:self];
        CGFloat x = startPoint.x;
        CGFloat y = startPoint.y;
        app_touch_move((int)x, (int)y, (int)t);
        i++;
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    int i = 0;
    for (UITouch *t in touches)
    {
        CGPoint startPoint = [t locationInView:self];
        CGFloat x = startPoint.x;
        CGFloat y = startPoint.y;
        app_touch_up((int)x, (int)y, (int)t);
        i++;
    }
}
- (void)accelerometer:(UIAccelerometer *)accelerometer 
        didAccelerate:(UIAcceleration *)acceleration
{
    app_accelerate((float)-acceleration.x, (float)-acceleration.y, (float)acceleration.z);
}
@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow *_window;
    GLView *_view;
}
@end

@implementation AppDelegate
- (void)dealloc
{
    [_window release];
    [super dealloc];
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    app_main();
    CGRect rect = [[UIScreen mainScreen] bounds];
    _window = [[UIWindow alloc] initWithFrame:rect];
    _window.backgroundColor = [UIColor redColor];
    _view = [[GLView alloc] initWithFrame:rect];
    [_window addSubview:_view];
    [_window makeKeyAndVisible];
    return YES;
}
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    app_pause();
}
@end

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
