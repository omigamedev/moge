#include "android_core.h"

void handle_input(AInputQueue *iq)
{
    AInputEvent* event;
    float x, y;
    int action, pindex, pcount, pid, i, event_type, handled = 0;

    if (AInputQueue_getEvent(iq, &event) >= 0)
    {
        // return if the event is not ready to be processed
        if (AInputQueue_preDispatchEvent(iq, event) != 0) return;

        event_type = AInputEvent_getType(event);
        if(event_type == AINPUT_EVENT_TYPE_MOTION)
        {
            action = AMotionEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK;
            pindex = (AMotionEvent_getAction(event) & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >>
                AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
            //LOG("event %d action %d index %d", AMotionEvent_getAction(event), action, pindex);
            switch(action)
            {
                case AMOTION_EVENT_ACTION_DOWN:
                    //LOG("AMOTION_EVENT_ACTION_DOWN");
                    x = AMotionEvent_getX(event, 0);
                    y = AMotionEvent_getY(event, 0);
                    pid = AMotionEvent_getPointerId(event, 0);
                    touch_add(pid, x, y);
                    app_touch_down(x, y, pid);
                    break;
                case AMOTION_EVENT_ACTION_POINTER_DOWN:
                    //LOG("AMOTION_EVENT_ACTION_POINTER_DOWN");
                    x = AMotionEvent_getX(event, pindex);
                    y = AMotionEvent_getY(event, pindex);
                    pid = AMotionEvent_getPointerId(event, pindex);
                    touch_add(pid, x, y);
                    app_touch_down(x, y, pid);
                    break;
                case AMOTION_EVENT_ACTION_UP:
                    //LOG("AMOTION_EVENT_ACTION_UP");
                    x = AMotionEvent_getX(event, 0);
                    y = AMotionEvent_getY(event, 0);
                    pid = AMotionEvent_getPointerId(event, 0);
                    touch_reset();
                    app_touch_up(x, y, pid);
                    break;
                case AMOTION_EVENT_ACTION_POINTER_UP:
                    //LOG("AMOTION_EVENT_ACTION_POINTER_UP");
                    x = AMotionEvent_getX(event, pindex);
                    y = AMotionEvent_getY(event, pindex);
                    pid = AMotionEvent_getPointerId(event, pindex);
                    touch_remove(pid);
                    app_touch_up(x, y, pid);
                    break;
                case AMOTION_EVENT_ACTION_MOVE:
                    //LOG("AMOTION_EVENT_ACTION_MOVE");
                    pcount = AMotionEvent_getPointerCount(event);
                    for(i = 0; i < pcount; i++)
                    {
                        x = AMotionEvent_getX(event, i);
                        y = AMotionEvent_getY(event, i);
                        pid = AMotionEvent_getPointerId(event, i);
                        if(touch_update(pid, x, y) == 1)
                            app_touch_move(x, y, pid);
                    }
                    break;
            }
        }
        else if(event_type == AINPUT_EVENT_TYPE_KEY)
        {
            int kaction = AKeyEvent_getAction(event);
            int kcode = AKeyEvent_getKeyCode(event);
//          int kcount = AKeyEvent_getRepeatCount(event);
//          int kflags = AKeyEvent_getFlags(event);
//          int kscan = AKeyEvent_getScanCode(event);
//          int kmeta = AKeyEvent_getMetaState(event);
//          int dtime = AKeyEvent_getDownTime(event);
//          LOG("count=%d flags=%d scan=%d meta=%d dtime=%d", kcount, kflags, kscan, kmeta, dtime);
            switch(kaction)
            {
            case AKEY_EVENT_ACTION_DOWN:
                handled = app_key_down(kcode);
                break;
            case AKEY_EVENT_ACTION_UP:
                handled = app_key_up(kcode);
                break;
            }
        }
        AInputQueue_finishEvent(iq, event, handled);
    }
}
