#ifndef _GFX_3DMATH_
#define _GFX_3DMATH_

#include <stdlib.h>

float clamp(float f, float min, float max);
float clampc(float c);
float interpolate_linear_in(float t, float a, float b);
float interpolate_linear_out(float t, float a, float b);
float interpolate_quadratic_in(float t, float a, float b);
float interpolate_quadratic_out(float t, float a, float b);
float randf();
float randfneg();

class gfxVector2D
{
public:
	float x, y;

	gfxVector2D();
	gfxVector2D(float xy);
	gfxVector2D(float x, float y);
};

class gfxScreenCoord
{
public:
	int x, y;
	
	gfxScreenCoord();
	gfxScreenCoord(int x, int y);
};

class gfxTexCoord
{
public:
	float u, v;

	gfxTexCoord();
	gfxTexCoord(float uv);
	gfxTexCoord(float u, float v);
};

class gfxColor
{
public:
	float r, g, b, a;

	gfxColor();
	gfxColor(float r, float g, float b, float a);
	gfxColor(int r, int g, int b, int a);
};

#endif
