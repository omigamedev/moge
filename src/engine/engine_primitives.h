#ifndef _GFX_PRIMITIVES_
#define _GFX_PRIMITIVES_

#include "engine_3dmath.h"
#include "engine_texture.h"

class gfxObject
{
public:
	gfxVector2D pos;

	gfxObject();
	virtual ~gfxObject();

	virtual void draw() = 0;
};

class gfxPlane : public gfxObject
{
public:
	gfxTexture tex;
	float rot;
	float width;
	float height;
	char visible;
	
	gfxPlane();
	virtual ~gfxPlane();
	
	virtual void draw();
};

class gfxSprite : public gfxObject
{
public:
	gfxTexture tex;
	float rot;
	float scale;
	float ratio;
	char visible;

	gfxSprite();
	virtual ~gfxSprite();

	virtual void draw();
	void load(const char *filename);
	virtual void set_texture(gfxTexture *t);
	virtual float width();
	virtual float height();
};

class gfxSpriteMultipart : public gfxSprite
{
public:
	gfxTexCoord tshift, tscale;

	virtual void draw();
	void set_region(int x, int y, int w, int h);

	gfxSpriteMultipart();
};

class gfxBitmapFont : public gfxSpriteMultipart
{
public:
	int rows;
	int cols;
	float cellw;
	float cellh;

	gfxBitmapFont();

	virtual void load(const char *filename);
	virtual void load(const char *filename, int w, int h);
	virtual void draw_text(const char *text, float x, float y);
	virtual void draw_text_center(const char *text, float x, float y);
	virtual void draw_text_right(const char *text, float x, float y);
	virtual float text_width(const char *text);
};

class gfxSpritesCollection
{
public:
	int count;
	gfxSpriteMultipart sprites[50];
	gfxTexture tex;
	char names[50][32];
	
	void load(const char *name, char *base);
	void get(const char *name, gfxSpriteMultipart &output);
};

class gfx9Slice : public gfxSpriteMultipart
{
public:
	int paddingx;
	int paddingy;
	float sx;
	float sy;

	gfx9Slice();
	virtual ~gfx9Slice();

	virtual void load(const char *filename);
	virtual void draw();
};

#define BLOB_RESOLUTION 24
#define BLOB_PIVOTS 4

class gfxBlob : public gfxSprite
{
public:
	gfxVector2D blob_v[BLOB_RESOLUTION + 2];
	gfxTexCoord blob_t[BLOB_RESOLUTION + 2];
	gfxVector2D blob_p[BLOB_PIVOTS];
	float blob_angle[BLOB_PIVOTS];
	float blob_dist[BLOB_PIVOTS];
	float blob_vel[BLOB_PIVOTS];
	float blob_vangle[BLOB_RESOLUTION];
	float blob_vdist[BLOB_RESOLUTION];
	float blob_div[BLOB_RESOLUTION];
	int blob_pi[BLOB_PIVOTS]; // pivot indexes
	
	void init();
	void draw();
};

template <class T>
class gfxBuffer
{
public:
	T *v;
	char *map;
	int max;
	int count;
	
	gfxBuffer()
	{
		v = 0;
		map = 0;
		max = 0;
		count = 0;
	}
	
	T *alloc()
	{
		for(int i = 0; i < max; i++) 
			if(map[i] == 0)
			{
				map[i] = 1;
				v[i] = T();	// initialize object
				count++;
				return &v[i];
			}
		return 0;
	}
	T *alloc(T &copy)
	{
		for(int i = 0; i < max; i++) 
			if(map[i] == 0)
			{
				map[i] = 1;
				v[i] = copy; // copy object
				count++;
				return &v[i];
			}
		return 0;
	}
	void free(T* obj)
	{
		// TODO: subtract pointers to find index: i = obj - v
		for(int i = 0; i < max; i++) 
			if(&v[i] == obj)
				map[i] = 0;
		count--;
	}
	void free(int index)
	{
		map[index] = 0;
		count--;
	}
	int indexof(T* obj)
	{
		for(int i = 0; i < max; i++) 
			if(&v[i] == obj) return i;
		return -1;
	}
	void create_buffers(int max)
	{
		this->max = max;
		this->v = new T[max];
		this->map = new char[max];
		reset();
	}
	void reset()
	{
		memset(map, 0, max);
		count = 0;
	}
	void draw()
	{
		for(int i = 0; i < max; i++) 
			if(map[i]) 
				v[i].draw();
	}
};

#endif
