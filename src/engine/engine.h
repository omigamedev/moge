#ifndef _ENGINE_H
#define _ENGINE_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <time.h>

#include <png/png.h>

#ifdef ENGINE_IOS

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#endif

#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <OpenAL/al.h>
#import <OpenAL/alc.h>

#define LOG(fmt, ...) NSLog((@"%s:%d " fmt), __PRETTY_FUNCTION__,\
    __LINE__, ##__VA_ARGS__)

#elif ENGINE_ANDROID

#include <GLES/gl.h>
#include <GLES/glext.h>
#include <EGL/egl.h>

#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_Android.h>

#include <android/native_activity.h>
#include <android/log.h>
#include <android/looper.h>
#include <android/configuration.h>
#include <android/sensor.h>

#define LOG(fmt,...) __android_log_print(ANDROID_LOG_INFO, "native",\
    "%s:%d "fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define ERR(fmt,...) __android_log_print(ANDROID_LOG_ERROR, "native",\
    "%s:%d "fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define TRUE 1
#define FALSE 0

#endif

typedef struct engine_video_t
{
    int screen_width;
    int screen_height;
} engine_video_t;

// life cycle
extern void app_main(void);
extern void app_init(engine_video_t *video);
extern void app_draw(int ms);
extern void app_pause(void);
extern void app_destroy(void);
// touch
extern void app_touch_down(int x, int y, int tid);
extern void app_touch_move(int x, int y, int tid);
extern void app_touch_up(int x, int y, int tid);
// key
extern int  app_key_down(int code);
extern int  app_key_up(int code);
// sensor
extern void app_accelerate(float x, float y, float z);

// system
GLuint engine_texture_load_png(FILE *fp, int *width, int *height);
FILE* engine_asset_open(const char *filename);
long engine_time_getms(void);
void engine_terminate();

// audio
int engine_audio_music_stop(int id);
int engine_audio_music_play(int id, int loop, float volume);
int engine_audio_music_load(const char *filename);
int engine_audio_init(void);
int engine_audio_destroy(void);
int engine_audio_volume_set(float volume);
int engine_audio_volume_offset(float volume);
int engine_audio_sound_load(const char *filename);
int engine_audio_sound_play(int id, float volume);

#ifdef __cplusplus
}
#endif

#endif
