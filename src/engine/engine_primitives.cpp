#include "engine_primitives.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

static float triangleCoords[] = {-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, -1.0f, 1.0f};
static float triangleTex[32];

/*OBJECT**********************************************************************/

gfxObject::gfxObject(){}
gfxObject::~gfxObject(){}
void gfxObject::draw(){}

/*PLANE***********************************************************************/

gfxPlane::gfxPlane() : visible(1), width(0.0f), height(0.0f), rot(0.0f){}
gfxPlane::~gfxPlane()
{
	//TODO: is safe to unload the texture? May be used outside. (seems its not)
	//tex.unload();
}

void gfxPlane::draw()
{
	if(!visible) return;
	
	triangleTex[0] = 0.0f;
	triangleTex[1] = 1.0f;
	triangleTex[2] = 1.0f;
	triangleTex[3] = 1.0f;
	triangleTex[4] = 1.0f;
	triangleTex[5] = 0.0f;
	triangleTex[6] = 0.0f;
	triangleTex[7] = 0.0f;
	
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
	glPushMatrix();
    glTranslatef(pos.x, pos.y, 0.0f);
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
    glScalef(width, height, 1.0f);
	
	glBindTexture(GL_TEXTURE_2D, tex.id);
    
	glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glPopMatrix();
	
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*SPRITE**********************************************************************/

gfxSprite::gfxSprite() : rot(0.0f), scale(1.0f), ratio(1.0f), visible(1){}
gfxSprite::~gfxSprite()
{
	//TODO: is safe to unload the texture? May be used outside. (seems its not)
	//tex.unload();
}

float gfxSprite::width(){ return scale; }
float gfxSprite::height(){ return scale * ratio; }

void gfxSprite::load(const char *filename)
{
	tex.load(filename);
	ratio = (float)tex.height / (float)tex.width;
}

void gfxSprite::set_texture(gfxTexture *t)
{
	tex = *t;
	ratio = (float)tex.height / (float)tex.width;
}

void gfxSprite::draw()
{
	if(!visible) return;
	
	triangleTex[0] = 0.0f;
	triangleTex[1] = 1.0f;
	triangleTex[2] = 1.0f;
	triangleTex[3] = 1.0f;
	triangleTex[4] = 1.0f;
	triangleTex[5] = 0.0f;
	triangleTex[6] = 0.0f;
	triangleTex[7] = 0.0f;

    glPushMatrix();
    glTranslatef(pos.x, pos.y, 0.0f);
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
    glScalef(scale, scale * ratio, 1.0f);

    glBindTexture(GL_TEXTURE_2D, (GLuint)tex.id);

    glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glPopMatrix();
}

/*MULTI-SPRITE****************************************************************/

gfxSpriteMultipart::gfxSpriteMultipart()
{
	tscale = gfxTexCoord(1.0f, 1.0f);
}

void gfxSpriteMultipart::draw()
{
	if(!visible) return;
	
	triangleTex[0] = tshift.u;
	triangleTex[1] = tscale.v + tshift.v;
	triangleTex[2] = tscale.u + tshift.u;
	triangleTex[3] = tscale.v + tshift.v;
	triangleTex[4] = tscale.u + tshift.u;
	triangleTex[5] = tshift.v;
	triangleTex[6] = tshift.u;
	triangleTex[7] = tshift.v;

    glPushMatrix();
    glTranslatef(pos.x, pos.y, 0.0f);
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
    glScalef(scale, scale * ratio, 1.0f);

    glBindTexture(GL_TEXTURE_2D, (GLuint)tex.id);

    glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glPopMatrix();
}

void gfxSpriteMultipart::set_region(int x, int y, int w, int h)
{
	if((tex.width | tex.height) == 0) return;
	tshift = gfxTexCoord((float)x / (float)tex.width, (float)y / (float)tex.height);
	tscale = gfxTexCoord((float)w / (float)tex.width, (float)h / (float)tex.height);
	ratio = (float)h / (float)w;
}

/*BITMAP-FONT*****************************************************************/

gfxBitmapFont::gfxBitmapFont()
{
}

void gfxBitmapFont::load(const char *filename)
{

}

void gfxBitmapFont::load(const char *filename, int w, int h)
{
	tex.load(filename);
	this->cols = floor((float)tex.width / (float)w);
	this->rows = floor((float)tex.height / (float)h);
	cellw = (float)w / (float)tex.width;
	cellh = (float)h / (float)tex.height;
	ratio = (float)h / (float)w;
}

void gfxBitmapFont::draw_text(const char *text, float x, float y)
{
    int l = strlen(text);

    pos.x = x;
	pos.y = y;
	float w = 2.0f;
	tscale = gfxTexCoord(cellw, cellh);

    glPushMatrix();

	glTranslatef(pos.x, pos.y, 0.0f);
	glRotatef(rot, 0.0f, 0.0f, 1.0f);
	glScalef(scale, scale * ratio, 1.0f);

    glBindTexture(GL_TEXTURE_2D, (GLuint)tex.id);

    glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);

	for(int i = 0; i < l; i++)
	{
		int c = text[i] - 32;
		if(text[i] == '\n')
		{
			pos.y -= height() * 2.0f;
			glPopMatrix();
			glPushMatrix();
			glTranslatef(x, pos.y, 0.0f);
			glRotatef(rot, 0.0f, 0.0f, 1.0f);
			glScalef(scale, scale * ratio, 1.0f);
		}
		else if(text[i] == '\r')
		{
			glPopMatrix();
			glPushMatrix();
		    glTranslatef(x, pos.y, 0.0f);
		    glRotatef(rot, 0.0f, 0.0f, 1.0f);
		    glScalef(scale, scale * ratio, 1.0f);
		}
		else if(c >= 0)
		{
		    float cx = (float)(c % cols);
			float cy = floor((float)c / (float)cols);
			tshift = gfxTexCoord(cx * cellw, cy * cellh);

			triangleTex[0] = tshift.u;
			triangleTex[1] = tscale.v + tshift.v;
			triangleTex[2] = tscale.u + tshift.u;
			triangleTex[3] = tscale.v + tshift.v;
			triangleTex[4] = tscale.u + tshift.u;
			triangleTex[5] = tshift.v;
			triangleTex[6] = tshift.u;
			triangleTex[7] = tshift.v;

		    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		    glTranslatef(w, 0.0f, 0.0f);
		}
	}
    glPopMatrix();
}

void gfxBitmapFont::draw_text_center(const char *text, float x, float y)
{
	draw_text(text, x - (float)(strlen(text) - 1) * scale, y);
}

void gfxBitmapFont::draw_text_right(const char *text, float x, float y)
{
	draw_text(text, x - (float)(strlen(text) - 1) * scale * 2.0f, y);
}

float gfxBitmapFont::text_width(const char *text)
{
	int l = strlen(text);
	return l * scale * 2.0f;
}

/*9-SLICE*********************************************************************/

gfx9Slice::gfx9Slice()
{
	sx = 1.0f;
	sy = 1.0f;
	paddingx = paddingy = 10;
	tscale = gfxTexCoord(1.0f, 1.0f);
}

gfx9Slice::~gfx9Slice()
{
	//TODO: is safe to unload the texture? May be used outside. (seems its not)
	//tex.unload();
}

void gfx9Slice::load(const char *filename)
{
	tex.load(filename);
}

void gfx9Slice::draw()
{
	if(!visible) return;
	
	float padx = (float)paddingx / (float)tex.width;
	float pady = (float)paddingy / (float)tex.height;

	float ipadx = 0.1f;//(float)tex.width / (float)paddingx;
	float ipady = 0.1f;//(float)tex.height / (float)paddingy;

	float sx = this->sx / scale;
	float sy = this->sy / scale;

	float triangleCoords[] = {
		-sx, sy,
		-sx+ipadx, sy,
		sx-ipadx, sy,
		sx, sy,

		-sx, sy-ipady,
		-sx+ipadx, sy-ipady,
		sx-ipadx, sy-ipady,
		sx, sy-ipady,

		-sx, -sy+ipady,
		-sx+ipadx, -sy+ipady,
		sx-ipadx, -sy+ipady,
		sx, -sy+ipady,

		-sx, -sy,
		-sx+ipadx, -sy,
		sx-ipadx, -sy,
		sx, -sy,
	};

	float u0 = tshift.u, 					v0 = tshift.v;
	float u1 = tshift.u + padx, 			v1 = tshift.v + pady;
	float u2 = tshift.u + tscale.u - padx,	v2 = tshift.v + tscale.v - pady;
	float u3 = tshift.u + tscale.u, 		v3 = tshift.v + tscale.v;

	float triangleTex[] = {
		u0, v0,
		u1, v0,
		u2, v0,
		u3, v0,

		u0, v1,
		u1, v1,
		u2, v1,
		u3, v1,

		u0, v2,
		u1, v2,
		u2, v2,
		u3, v2,

		u0, v3,
		u1, v3,
		u2, v3,
		u3, v3,
	};
	GLubyte triangleIndex[] = {
		4,5,1, 1,0,4,
		5,6,2, 2,1,5,
		6,7,3, 3,2,6,

		8,9,5, 5,4,8,
		9,10,6, 6,5,9,
		10,11,7, 7,6,10,

		12,13,9, 9,8,12,
		13,14,10, 10,9,13,
		14,15,11, 11,10,14
	};

	glPushMatrix();

	glBindTexture(GL_TEXTURE_2D, (GLuint)tex.id);

    glTranslatef(pos.x, pos.y, 0.0f);
    glRotatef(rot, 0.0f, 0.0f, 1.0f);
    glScalef(scale, scale * ratio, 1.0f);

	glVertexPointer(2, GL_FLOAT, 0, triangleCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, triangleTex);
	glDrawElements(GL_TRIANGLES, 54, GL_UNSIGNED_BYTE, triangleIndex);

	glPopMatrix();
}

/*BLOB***********************************************************************/

void gfxBlob::init()
{
	srand(time(0));
	// init blob pivots
	int index_step = BLOB_RESOLUTION / BLOB_PIVOTS;
	float angle_step = M_PI * 2.0f / BLOB_PIVOTS;
	for(int i = 0; i < BLOB_PIVOTS; i++)
	{
		blob_pi[i] = i * index_step;
		blob_p[i].x = sinf(i * angle_step);
		blob_p[i].y = cosf(i * angle_step);
		blob_t[i].u = blob_p[i].x;
		blob_t[i].v = blob_p[i].y;
		blob_angle[i] = i * angle_step;
		blob_vel[i] = (rand() % 1000) * 0.0001f + 0.1;
		blob_dist[i] = 1.0f;
	}
	//blob_dist[0] = 0;
	//blob_vel[0] = 0.1f;
	angle_step = M_PI * 2.0f / BLOB_RESOLUTION;
	for(int i = 0; i < BLOB_RESOLUTION + 2; i++)
	{
		blob_t[i].u = (sinf(i * angle_step) + 1.0f) * 0.5f;
		blob_t[i].v = (cosf(i * angle_step) + 1.0f) * 0.5f;
	}
	blob_t[0].u = blob_t[0].v = 0.5f;
}

void gfxBlob::draw()
{
	// animate blob pivots
	float angle_step = M_PI * 2.0f / BLOB_PIVOTS;
	for(int i = 0; i < BLOB_PIVOTS; i++)
	{
		float phase = sinf(blob_angle[i]) * 0.1f + blob_dist[i];
		blob_p[i].x = sinf(angle_step * i) * (phase);
		blob_p[i].y = cosf(angle_step * i) * (phase);
		blob_angle[i] += blob_vel[i];
	}
	
	// animate blob
	memset(blob_div, 0, sizeof(blob_div));
	memset(blob_vangle, 0, sizeof(blob_vangle));
	memset(blob_vdist, 0, sizeof(blob_vdist));
	for(int i = 0; i < BLOB_PIVOTS; i++)
	{
		int ii = blob_pi[i];
		for(int j = 0; j < 6; j++)
		{
			int mul = (6-j) * 3;
			int jp = (ii - j) % BLOB_RESOLUTION;
			if(jp < 0) jp = BLOB_RESOLUTION + jp;
			int jn = (ii + j) % BLOB_RESOLUTION;
			blob_div[jp] += mul;
			blob_vangle[jp] += sinf(blob_angle[i]) * mul;
			blob_vdist[jp] += blob_dist[i] * mul;
			if(jp != jn)
			{
				blob_div[jn] += mul;
				blob_vangle[jn] += sinf(blob_angle[i]) * mul;
				blob_vdist[jn] += blob_dist[i] * mul;
			}
		}
	}
	
	angle_step = M_PI * 2.0f / BLOB_RESOLUTION;
	for(int i = 0; i < BLOB_RESOLUTION; i++)
	{
		float phase = blob_vangle[i] / blob_div[i] * 0.1f;
		float dist = blob_vdist[i] / blob_div[i];
		blob_v[i + 1].x = sinf(angle_step * i) * (dist + phase);
		blob_v[i + 1].y = cosf(angle_step * i) * (dist + phase);
	}
	blob_v[BLOB_RESOLUTION + 1] = blob_v[1];
	
	// draw blob
	glPushMatrix();
	
	glTranslatef(pos.x, pos.y, 0.0f);
	glRotatef(rot, 0.0f, 0.0f, 1.0f);
	glScalef(scale, scale * ratio, 1.0f);
	
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	
	glBindTexture(GL_TEXTURE_2D, tex.id);
	glVertexPointer(2, GL_FLOAT, 0, blob_v);
	glTexCoordPointer(2, GL_FLOAT, 0, blob_t);
	glPointSize(1.0f);
	glDrawArrays(GL_TRIANGLE_FAN, 0, BLOB_RESOLUTION + 2);
	
//	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
//	glBindTexture(GL_TEXTURE_2D, 0);
//	glVertexPointer(2, GL_FLOAT, 0, blob_p);
//	glPointSize(4.0f);
//	glDrawArrays(GL_POINTS, 0, BLOB_PIVOTS);
//	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	
	glPopMatrix();
}

/*SPRITES-COLLECTION*************************************************************/

void gfxSpritesCollection::load(const char *name, char *base)
{
	char project[256];
	char sprite[256];
	sprintf(project, "%s/%s.txt", base, name);
	sprintf(sprite, "%s.png", name);
	tex.load(sprite);
	FILE *fp = fopen(project, "r");
	int x, y, w, h;
	count = 0;
	if(fp)
	{
		int n = 5;
		while(n == 5)
		{
			n = fscanf(fp, "%s %d %d %d %d\n", names[count], &x, &y, &w, &h);
			if(n == 5)
			{
				sprites[count].set_texture(&tex);
				sprites[count].set_region(x, y, w, h);
				count++;
			}
		}
	}
	else
	{
		LOG("cant load %s", project);
	}
}

void gfxSpritesCollection::get(const char *name, gfxSpriteMultipart &output)
{
	char s[32];
	sprintf(s, "%s.png", name);
	for(int i = 0; i < count; i++)
	{
		if(strcmp(s, names[i]) == 0)
		{
			output.tex = sprites[i].tex;
			output.rot = sprites[i].rot;
			output.scale = sprites[i].scale;
			output.ratio = sprites[i].ratio;
			output.visible = sprites[i].visible;
			output.tscale = sprites[i].tscale;
			output.tshift = sprites[i].tshift;
			output.rot = sprites[i].rot;
			output.rot = sprites[i].rot;
			return;
		}
	}
}
