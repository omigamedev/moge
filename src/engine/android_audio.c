#include "android_core.h"

#define VOLUME_MIN (-6000)
#define VOLUME_MAX (0)

#define BUFFER_MAX 32
#define PLAYER_MAX 32
static android_sound_buffer_t sbuffer[BUFFER_MAX];
static android_sound_player_t splayer[PLAYER_MAX];
static android_music_t mbuffer;
static int buffer_index;

static SLObjectItf sndEngineObj, sndOutMixObj;
static SLEngineItf sndEngine;
static float g_volume;
//static SLVolumeItf sndVolume;
//static SLDeviceVolumeItf sndDevVolume;
//static SLOutputMixItf sndOutMix;
extern AAssetManager *android_am;

#define ASSERT(exp){SLresult r=exp; if(r!=SL_RESULT_SUCCESS){LOG("SLError %s",r2s(r));goto SLERROR;}}

const char* r2s(SLresult r)
{
    switch(r)
    {
    case SL_RESULT_SUCCESS: return "SL_RESULT_SUCCESS"; break;
    case SL_RESULT_PRECONDITIONS_VIOLATED: return "SL_RESULT_PRECONDITIONS_VIOLATED"; break;
    case SL_RESULT_PARAMETER_INVALID: return "SL_RESULT_PARAMETER_INVALID"; break;
    case SL_RESULT_MEMORY_FAILURE: return "SL_RESULT_MEMORY_FAILURE"; break;
    case SL_RESULT_RESOURCE_ERROR: return "SL_RESULT_RESOURCE_ERROR"; break;
    case SL_RESULT_RESOURCE_LOST: return "SL_RESULT_RESOURCE_LOST"; break;
    case SL_RESULT_IO_ERROR: return "SL_RESULT_IO_ERROR"; break;
    case SL_RESULT_BUFFER_INSUFFICIENT: return "SL_RESULT_BUFFER_INSUFFICIENT"; break;
    case SL_RESULT_CONTENT_CORRUPTED: return "SL_RESULT_CONTENT_CORRUPTED"; break;
    case SL_RESULT_CONTENT_UNSUPPORTED: return "SL_RESULT_CONTENT_UNSUPPORTED"; break;
    case SL_RESULT_CONTENT_NOT_FOUND: return "SL_RESULT_CONTENT_NOT_FOUND"; break;
    case SL_RESULT_PERMISSION_DENIED: return "SL_RESULT_PERMISSION_DENIED"; break;
    case SL_RESULT_FEATURE_UNSUPPORTED: return "SL_RESULT_FEATURE_UNSUPPORTED"; break;
    case SL_RESULT_INTERNAL_ERROR: return "SL_RESULT_INTERNAL_ERROR"; break;
    case SL_RESULT_UNKNOWN_ERROR: return "SL_RESULT_UNKNOWN_ERROR"; break;
    case SL_RESULT_OPERATION_ABORTED: return "SL_RESULT_OPERATION_ABORTED"; break;
    case SL_RESULT_CONTROL_LOST: return "SL_RESULT_CONTROL_LOST"; break;
    };
    return "UNKNOWN";
}

int engine_audio_init()
{
    SLAudioIODeviceCapabilitiesItf cap;
    SLint32 ndev = 5;
    SLuint32 ids[5];
    SLOutputMixItf outmix;
    
    SLInterfaceID engineIID[] = {SL_IID_ENGINE, SL_IID_DEVICEVOLUME};
    SLboolean engineBool[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    ASSERT(slCreateEngine(&sndEngineObj, 0, NULL, (SLint32)1, engineIID, engineBool));
    ASSERT((*sndEngineObj)->Realize(sndEngineObj, SL_BOOLEAN_FALSE));
    ASSERT((*sndEngineObj)->GetInterface(sndEngineObj, SL_IID_ENGINE, &sndEngine));

    SLInterfaceID mixIID[] = {SL_IID_OUTPUTMIX, SL_IID_VOLUME};
    SLboolean mixBool[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    ASSERT((*sndEngine)->CreateOutputMix(sndEngine, &sndOutMixObj, (SLint32)1, mixIID, mixBool));
    ASSERT((*sndOutMixObj)->Realize(sndOutMixObj, SL_BOOLEAN_FALSE));
    ASSERT((*sndOutMixObj)->GetInterface(sndOutMixObj, SL_IID_OUTPUTMIX, &outmix));
    
    memset(&mbuffer, 0, sizeof(mbuffer));
    memset(sbuffer, 0, sizeof(sbuffer));
    memset(splayer, 0, sizeof(splayer));
    
    g_volume = 1.0f;
    buffer_index = 0;
    LOG("audio initted");
    return 1;
SLERROR:
    LOG("sl error");
    return 0;
}

float volume_clamp(float volume)
{
    if(volume < 0.0f) return 0.0f;
    if(volume > 1.0f) return 1.0f;
    return volume;
}

int engine_audio_volume_offset(float volume)
{
    return engine_audio_volume_set(g_volume + volume);
}

int engine_audio_volume_set(float volume)
{
    g_volume = volume_clamp(volume);
    
    SLuint32 pstate;
    if(mbuffer.player != NULL)
    {
        ASSERT((*mbuffer.player)->GetPlayState(mbuffer.player, &pstate));
        if(pstate != SL_PLAYSTATE_STOPPED)
        {
            // TODO:BUG: setting low volume set player state to PAUSED
            ASSERT((*mbuffer.volume)->SetVolumeLevel(mbuffer.volume, 
                (SLmillibel)((float)VOLUME_MIN * (1.0f - mbuffer.fvol * g_volume))));
            //LOG("set player volume %d millibel",
            //  (SLmillibel)(VOLUME_MIN * (1.0f - mbuffer.fvol * g_volume)));
        }
    }
    
    int i, empty = -1;
    SLBufferQueueState state;
    for(i = 0; i < BUFFER_MAX; i++)
    {
        if(splayer[i].queue)
        {
            ASSERT((*splayer[i].queue)->GetState(splayer[i].queue, &state));
            // if playing set volume
            if(state.count > 0)
            {
                ASSERT((*splayer[i].volume)->SetVolumeLevel(splayer[i].volume, 
                    VOLUME_MIN * (1.0f - splayer[i].fvol * g_volume)));
            }
        }
    }
    
    return TRUE;
SLERROR:
    return FALSE;
}

int engine_audio_destroy()
{
    int i;
    // clear all raw buffers
    for(i = 0; i < buffer_index; i++)
    {
        free(sbuffer[i].data);
    }
    // clear players
    for(i = 0; i < BUFFER_MAX; i++)
    {
        if(splayer[i].player)
        {
            (*splayer[i].player_obj)->Destroy(splayer[i].player_obj);
        }
    }
    // clear the rest
    if(mbuffer.player_obj) (*mbuffer.player_obj)->Destroy(mbuffer.player_obj);
    if(sndOutMixObj) (*sndOutMixObj)->Destroy(sndOutMixObj);
    if(sndEngineObj) (*sndEngineObj)->Destroy(sndEngineObj);
    LOG("audio destroyed");
    return 1;
}

int engine_audio_music_load(const char *filename)
{
    long off, len;
    int fd = android_asset_fd(filename, &off, &len);
    if(fd < 0) return 0;

    SLDataLocator_AndroidFD sndDataLocatorIn;
    sndDataLocatorIn.locatorType = SL_DATALOCATOR_ANDROIDFD;
    sndDataLocatorIn.fd = fd;
    sndDataLocatorIn.offset = off;
    sndDataLocatorIn.length = len;

    SLDataFormat_MIME sndFormat;
    sndFormat.containerType = SL_CONTAINERTYPE_UNSPECIFIED;
    sndFormat.formatType = SL_DATAFORMAT_MIME;
    sndFormat.mimeType = NULL;

    SLDataSource sndDataSource;
    sndDataSource.pFormat = NULL;
    sndDataSource.pLocator = &sndDataLocatorIn;

    SLDataLocator_OutputMix sndDataLocatorOut;
    sndDataLocatorOut.locatorType = SL_DATALOCATOR_OUTPUTMIX;
    sndDataLocatorOut.outputMix = sndOutMixObj;

    SLDataSink sndSink;
    sndSink.pLocator = &sndDataLocatorOut;
    sndSink.pFormat = NULL;

    SLInterfaceID playerIID[] = {SL_IID_PLAY, SL_IID_SEEK, SL_IID_VOLUME};
    SLboolean playerBool[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    ASSERT((*sndEngine)->CreateAudioPlayer(sndEngine, &mbuffer.player_obj, &sndDataSource, &sndSink,
        (SLuint32)3, playerIID, playerBool));
    ASSERT((*mbuffer.player_obj)->Realize(mbuffer.player_obj, SL_BOOLEAN_FALSE));
    ASSERT((*mbuffer.player_obj)->GetInterface(mbuffer.player_obj, SL_IID_PLAY, &mbuffer.player));
    ASSERT((*mbuffer.player_obj)->GetInterface(mbuffer.player_obj, SL_IID_SEEK, &mbuffer.seek));
    ASSERT((*mbuffer.player_obj)->GetInterface(mbuffer.player_obj, SL_IID_VOLUME, &mbuffer.volume));
    
    return 1;
SLERROR:
    return 0;
}

int engine_audio_music_play(int id, int loop, float volume)
{
    float v = volume_clamp(volume);
    ASSERT((*mbuffer.volume)->SetVolumeLevel(mbuffer.volume, VOLUME_MIN * (1.0f - v * g_volume)));
    mbuffer.fvol = v;
    ASSERT((*mbuffer.seek)->SetLoop(mbuffer.seek, loop ? SL_BOOLEAN_TRUE : SL_BOOLEAN_FALSE, 0, SL_TIME_UNKNOWN));
    ASSERT((*mbuffer.player)->SetPlayState(mbuffer.player, SL_PLAYSTATE_PLAYING));
    return 1;
SLERROR:
    return 0;
}

int engine_audio_music_stop(int id)
{
    ASSERT((*mbuffer.player)->SetPlayState(mbuffer.player, SL_PLAYSTATE_STOPPED));
    return 1;
SLERROR:
    return 0;
}

int engine_audio_sound_load(const char *filename)
{
    long off, len;
    android_sound_buffer_t buffer;
    AAsset *as = AAssetManager_open(android_am, filename, AASSET_MODE_STREAMING);
    int fd = AAsset_openFileDescriptor(as, &off, &len);
    if (fd >= 0)
    {
        buffer.size = len;
        buffer.data = malloc(sizeof(SLuint8) * len);
        int readlen = AAsset_read(as, buffer.data, len);
    }
    else
    {
        buffer.size = AAsset_getLength(as);
        buffer.data = malloc(sizeof(SLuint8) * buffer.size);
        const void *buf = AAsset_getBuffer(as);
        memcpy(buffer.data, buf, buffer.size);
    }
    AAsset_close(as);
    int i = buffer_index++;
    sbuffer[i] = buffer;
    return i;
SLERROR:
    return -1;
}

int engine_audio_sound_buffer(unsigned char *data, long len)
{
    android_sound_buffer_t buffer;
    buffer.size = len;
    buffer.data = data;
    sbuffer[0] = buffer;
    return 0;
SLERROR:
    return -1;
}

int player_create(android_sound_player_t *out, int nbuffers)
{
    android_sound_player_t p;

    SLDataLocator_AndroidSimpleBufferQueue sndDataLocatorIn;
    sndDataLocatorIn.locatorType = SL_DATALOCATOR_ANDROIDSIMPLEBUFFERQUEUE;
    sndDataLocatorIn.numBuffers = nbuffers;

    SLDataFormat_PCM sndFormat;
    sndFormat.formatType = SL_DATAFORMAT_PCM;
    sndFormat.numChannels = 1;
    sndFormat.samplesPerSec = SL_SAMPLINGRATE_22_05;
    sndFormat.bitsPerSample = SL_PCMSAMPLEFORMAT_FIXED_16;
    sndFormat.containerSize = SL_PCMSAMPLEFORMAT_FIXED_16;
    sndFormat.channelMask = SL_SPEAKER_FRONT_CENTER;
    sndFormat.endianness = SL_BYTEORDER_LITTLEENDIAN;

    SLDataSource sndDataSource;
    sndDataSource.pFormat = &sndFormat;
    sndDataSource.pLocator = &sndDataLocatorIn;

    SLDataLocator_OutputMix sndDataLocatorOut;
    sndDataLocatorOut.locatorType = SL_DATALOCATOR_OUTPUTMIX;
    sndDataLocatorOut.outputMix = sndOutMixObj;

    SLDataSink sndSink;
    sndSink.pLocator = &sndDataLocatorOut;
    sndSink.pFormat = NULL;
    
    SLInterfaceID playerIID[] = {SL_IID_PLAY, SL_IID_BUFFERQUEUE, SL_IID_VOLUME};
    SLboolean playerBool[] = {SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE, SL_BOOLEAN_TRUE};
    ASSERT((*sndEngine)->CreateAudioPlayer(sndEngine, &p.player_obj, &sndDataSource, &sndSink, (SLuint32)3,
        playerIID, playerBool));
    ASSERT((*p.player_obj)->Realize(p.player_obj, SL_BOOLEAN_FALSE));
    ASSERT((*p.player_obj)->GetInterface(p.player_obj, SL_IID_PLAY, &p.player));
    ASSERT((*p.player_obj)->GetInterface(p.player_obj, SL_IID_BUFFERQUEUE, &p.queue));
    ASSERT((*p.player_obj)->GetInterface(p.player_obj, SL_IID_VOLUME, &p.volume));
    
    ASSERT((*p.player)->SetPlayState(p.player, SL_PLAYSTATE_PLAYING));
    
    *out = p;
    
    return 1;
SLERROR:
    return 0;
}

int player_get()
{
    SLBufferQueueState state;
    int i, empty = -1;
    for(i = 0; i < BUFFER_MAX; i++)
    {
        if(splayer[i].queue)
        {
            ASSERT((*splayer[i].queue)->GetState(splayer[i].queue, &state));
            if(state.count == 0)
            {
                return i;
            }
        }
        else if(empty == -1)
        {
            empty = i;
        }
    }
    if(empty > -1)
    {
        if(player_create(&splayer[empty], 1) == 0)
        {
            LOG("player_create error");
            return -1;
        }
        return empty;
    }
SLERROR:
    return -1;
}

int player_queue_end(android_sound_player_t *pl, SLBufferQueueState *st)
{
    //SLBufferQueueState state;
    ASSERT((*pl->queue)->GetState(pl->queue, st));
    return 1;
SLERROR:
    return 0;
}

int engine_queue_add(android_sound_player_t *pl, SLuint8 *data, SLuint32 size)
{
    SLBufferQueueState state;
    ASSERT((*pl->queue)->GetState(pl->queue, &state));
    if(state.count < 8)
    {
        ASSERT((*pl->queue)->Enqueue(pl->queue, data, size));
        LOG("index %d count %d", state.playIndex, state.count);
        return 1;
    }
//    else if(state.count - 1 == state.playIndex)
//    {
//        //ASSERT((*pl->queue)->Clear(pl->queue));
//        //ASSERT((*pl->queue)->Enqueue(pl->queue, data, size));
//        LOG("CLEAR index %d count %d", state.playIndex, state.count);
//        return 1;
//    }
    //LOG("INFO index %d count %d", state.playIndex, state.count);
    return 0;
SLERROR:
    return -1;
}

int engine_audio_sound_play(int id, float volume)
{
    int pid = player_get();
    if(pid < 0) return 0;
    float v = volume_clamp(volume);
    ASSERT((*splayer[pid].volume)->SetVolumeLevel(splayer[pid].volume, VOLUME_MIN * (1.0f - v * g_volume)));
    splayer[pid].fvol = v;
    ASSERT((*splayer[pid].queue)->Clear(splayer[pid].queue));
    ASSERT((*splayer[pid].queue)->Enqueue(splayer[pid].queue, sbuffer[id].data, sbuffer[id].size));
    return 1;
SLERROR:
    return 0;
}
