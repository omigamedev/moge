#include "engine_texture.h"

gfxTexture::gfxTexture() : id(0), width(0), height(0)
{
}

void gfxTexture::unload()
{
    glDeleteTextures(1, (GLuint*)&id);
}

void gfxTexture::load(const char *filename)
{
    FILE *fp;
    fp = engine_asset_open(filename);
    id = engine_texture_load_png(fp, &width, &height);
}
