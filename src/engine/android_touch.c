#include "android_core.h"

typedef struct touch_t
{
    int x;
    int y;
    int id;
} touch_t;
static touch_t touches[10];

int touch_add(int id, int x, int y)
{
    int i;
    for(i = 0; i < 10; i++)
    {
        if(touches[i].id == -1)
        {
            touches[i].x = x;
            touches[i].y = y;
            touches[i].id = id;
            return 1;
        }
    }
    return 0;
}

int touch_index(int id)
{
    int i;
    for(i = 0; i < 10; i++)
    {
        if(touches[i].id == id)
        {
            return i;
        }
    }
    return -1;
}

int touch_update(int id, int x, int y)
{
    int i;
    for(i = 0; i < 10; i++)
    {
        if(touches[i].id == id)
        {
            if(touches[i].x == x && touches[i].y == y)
                return 2;
            touches[i].x = x;
            touches[i].y = y;
            return 1;
        }
    }
    return 0;
}

int touch_remove(int id)
{
    int i;
    for(i = 0; i < 10; i++)
    {
        if(touches[i].id == id)
        {
            touches[i].id = -1;
            return 1;
        }
    }
    return 0;
}

void touch_reset()
{
    int i;
    for(i = 0; i < 10; i++)
    {
        touches[i].x = 0;
        touches[i].y = 0;
        touches[i].id = -1;
    }
}
