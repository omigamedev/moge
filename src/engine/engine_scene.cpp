#include "engine_scene.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/*SCENE-DELEGATE**************************************************************/
gfxSceneDelegate::~gfxSceneDelegate(){}
void gfxSceneDelegate::on_scene_delegate(class gfxScene *sender, GFX_SCENE_EVENT event){}

/*SCENE***********************************************************************/
gfxScene::gfxScene()
{
	anim_counter = 0;
	anim_duration = 0;
	nscene = 0;
	history_index = 0;
	subscenes = 0;
	nscene = 0;
	memset(history, 0, sizeof(history));
	memset(history_fx, 0, sizeof(history_fx));
	memset(history_durations, 0, sizeof(history_durations));
	memset(history_backgrounds, 0, sizeof(history_backgrounds));
}
gfxScene::~gfxScene(){}
void gfxScene::init(){}
void gfxScene::resume()
{
    int i;
    if(nscene > 0)
    {
        for(i = 0; i <= history_index; i++)
        {
            subscenes[history[i]]->resume();
        }
    }
}
void gfxScene::draw()
{
    if(nscene > 0)
    {
        for(int i = 0; i <= history_index; i++)
        {
            if(history_backgrounds[i])
            {
                subscenes[history[i]]->draw();
            }
        }
    }
	if(anim_counter < anim_duration)
	{
		anim_counter++;
		float t = (float)anim_counter / (float)anim_duration;
		switch(anim_fx)
		{
			case FX_SLIDE_O:
				transition_update_slide_o(t);
				break;
			case FX_SLIDE_V:
				transition_update_slide_v(t);
				break;
			case FX_FADE_BLACK:
				transition_update_fade_black(t);
				break;
			case FX_SWAP:
				transition_update_swap();
				break;
		}
	}
	else if(nscene > 0)
	{
		subscenes[history[history_index]]->draw();
	}
}
void gfxScene::destroy()
{
    int i;
    if(nscene > 0)
    {
        for(i = 0; i <= history_index; i++)
        {
            subscenes[history[i]]->destroy();
        }
    }
}
void gfxScene::enable(){}
void gfxScene::disable(){}
void gfxScene::add(gfxScene *scene)
{
	//scene->init();
	scene->delegate = this;
	subscenes[nscene++] = scene;
}
void gfxScene::remove(gfxScene *scene){}
void gfxScene::navigate(gfxScene *scene, GFX_SCENE_TRANSITION fx,
    int direction, int duration, int modal)
{
	int to = index_of(scene);
	if(to == -1) return;
	if(to != history[history_index])
	{
		anim_counter = 0;
		anim_duration = duration;
		anim_dir = direction;
		anim_fx = fx;
		
		history_index++;
		subscenes[history[history_index - 1]]->disable();
		history_backgrounds[history_index - 1] = modal;
		history[history_index] = to;
		history_fx[history_index] = fx;
		history_durations[history_index] = duration;
		subscenes[history[history_index]]->init();
	}
	transition_start();
}
int gfxScene::index_of(gfxScene *scene)
{
	for(int i = 0; i < nscene; i++)
	{
		if(subscenes[i] == scene) return i;
	}
	return -1;
}
void gfxScene::back(gfxScene *scene)
{
    int to = index_of(scene);
    if(to == -1) return;
    back(history_index - to);
}
void gfxScene::back(int n)
{
	if(history_index > 0 && n > 0)
	{
		anim_counter = 0;
		anim_duration = 10;
		anim_dir = -1;
		for(int i = 0; i < n-1; i++)
		{
		    subscenes[history[history_index]]->disable();
		    subscenes[history[history_index]]->destroy();
		    history_index--;
		    history_backgrounds[history_index] = 0;
		}
		subscenes[history[history_index]]->disable();
		anim_fx =  history_fx[history_index];
		anim_duration = history_durations[history_index];
	}
}
void gfxScene::transition_end(){}
void gfxScene::transition_start(){}
void gfxScene::transition_update_slide_o(float t)
{
	float d = 2.0f;
	subscenes[history[history_index - 1]]->pos.y = 0.0f;
	subscenes[history[history_index]]->pos.y = 0.0f;
	if(anim_dir == 1)
	{
		subscenes[history[history_index - 1]]->pos.x = interpolate_quadratic_out(t, 0.0f, -d);
		subscenes[history[history_index]]->pos.x = interpolate_quadratic_out(t, d, 0.0f);
	}
	else if(anim_dir == -1)
	{
		subscenes[history[history_index - 1]]->pos.x = interpolate_quadratic_out(t, -d, 0.0f);
		subscenes[history[history_index]]->pos.x = interpolate_quadratic_out(t, 0.0f, d);
	}
	if(anim_counter == anim_duration)
	{
	    if(anim_dir == -1)
	    {
	        subscenes[history[history_index]]->destroy();
	        history_index--;
	        history_backgrounds[history_index] = 0;
	    }
		subscenes[history[history_index]]->enable();
		transition_end();
	}
	else
	{
		subscenes[history[history_index - 1]]->draw();
	}
	subscenes[history[history_index]]->draw();
}
void gfxScene::transition_update_slide_v(float t)
{
	float d = 3.0f; //TODO: use real height
	subscenes[history[history_index - 1]]->pos.x = 0.0f;
	subscenes[history[history_index]]->pos.x = 0.0f;
	if(anim_dir == 1)
	{
		subscenes[history[history_index - 1]]->pos.y = interpolate_quadratic_out(t, 0.0f, d);
		subscenes[history[history_index]]->pos.y = interpolate_quadratic_out(t, -d, 0.0f);
	}
	else if(anim_dir == -1)
	{
		subscenes[history[history_index - 1]]->pos.y = interpolate_quadratic_out(t, d, 0.0f);
		subscenes[history[history_index]]->pos.y = interpolate_quadratic_out(t, 0.0f, -d);
	}
	if(anim_counter == anim_duration)
	{
	    if(anim_dir == -1)
	    {
	        subscenes[history[history_index]]->destroy();
	        history_index--;
	        history_backgrounds[history_index] = 0;
	    }
		subscenes[history[history_index]]->enable();
		transition_end();
	}
	else
	{
		subscenes[history[history_index - 1]]->draw();
	}
	subscenes[history[history_index]]->draw();
}
void gfxScene::transition_update_fade_black(float t)
{
//	subscenes[history[history_index]]->pos.y = 0.0f;
//	subscenes[history[history_index]]->pos.x = 0.0f;
	
	if(anim_counter < anim_duration / 2)
	{
//        subscenes[history[history_index - 1]]->pos.y = 0.0f;
//        subscenes[history[history_index - 1]]->pos.x = 0.0f;
		if(anim_dir == 1) subscenes[history[history_index - 1]]->draw();
		else if(anim_dir == -1) subscenes[history[history_index]]->draw();
	}
	else if(anim_counter == anim_duration / 2)
	{
	    //LOG("half index:%d history:%d scene:%p", history_index,
        //    history[history_index], subscenes[history[history_index]]);
	    if(anim_dir == -1)
	    {
	        subscenes[history[history_index]]->destroy();
	        history_index--;
	        history_backgrounds[history_index] = 0;
	    }
		subscenes[history[history_index]]->enable();
		transition_end();
	}
	else if(anim_counter > anim_duration / 2)
	{
		subscenes[history[history_index]]->draw();
	}
	
	float alpha = sinf(t * M_PI);
	glColor4f(0.0f, 0.0f, 0.0f, alpha);
	fader.width = 1.0f;
	fader.height = 2.0f; //TODO: use real height
	fader.draw();
}
void gfxScene::transition_update_swap()
{
	if(anim_dir == -1)
	{
        subscenes[history[history_index]]->destroy();
	    history_index--;
	    history_backgrounds[history_index] = 0;
	}
	subscenes[history[history_index]]->enable();
	anim_counter = anim_duration;
	transition_end();
}
