#ifndef _GFX_SCENE_
#define _GFX_SCENE_

#include "engine_3dmath.h"
#include "engine_texture.h"
#include "engine_primitives.h"

typedef int GFX_SCENE_EVENT;
typedef int GFX_SCENE_TRANSITION;

class gfxSceneDelegate
{
public:
	virtual ~gfxSceneDelegate();
	
	virtual void on_scene_delegate(class gfxScene *sender, GFX_SCENE_EVENT event);
};

class gfxScene : public gfxSceneDelegate
{
public:
	static const GFX_SCENE_TRANSITION FX_SWAP = 0;
	static const GFX_SCENE_TRANSITION FX_SLIDE_O = 1;
	static const GFX_SCENE_TRANSITION FX_SLIDE_V = 2;
	static const GFX_SCENE_TRANSITION FX_FADE_BLACK = 3;
	
	class gfxSceneDelegate *delegate;
	gfxVector2D pos;
	gfxScene **subscenes;
	gfxPlane fader;
	int history[10], history_index;
	GFX_SCENE_TRANSITION history_fx[10];
	int history_durations[10];
	int history_backgrounds[10]; // bg scenes for modal (bool)
	int nscene;
	int anim_counter, anim_duration, anim_dir, anim_fx;

	gfxScene();
	virtual ~gfxScene();
	
    virtual void init();
    virtual void resume();
	virtual void destroy();
	virtual void draw();
	virtual void enable();
	virtual void disable();
	virtual void transition_end();
	virtual void transition_start();
	
	void add(gfxScene *scene);
	void remove(gfxScene *scene);
	void navigate(gfxScene *scene, GFX_SCENE_TRANSITION fx = FX_SWAP,
        int direction = 1, int duration = 10, int modal = 0);
	void transition_update_slide_o(float t);
	void transition_update_slide_v(float t);
	void transition_update_fade_black(float t);
	void transition_update_swap();
	void back(int n = 1);
	void back(gfxScene *scene);
	
	int index_of(gfxScene *scene);
};

#endif
